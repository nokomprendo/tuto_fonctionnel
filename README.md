
![](images/logo_tuto_small.png)

# tuto_fonctionnel

## tuto sur la programmation fonctionnelle au sens large

- langage de programmation (Haskell...)
- gestionnaires de paquets (Nix, Guix...)
- distribution linux (NixOS...)
- ...


## ressources

- [les videos sur peertube](https://peertube.fr/video-channels/tuto_fonctionnel/videos)
- [les videos sur youtube](https://www.youtube.com/channel/UCqxytIRrg8LRl31QuXDXf2A/videos)
- [les articles](https://nokomprendo.frama.io/tuto_fonctionnel)
- [le dépôt git](https://framagit.org/nokomprendo/tuto_fonctionnel)


## génération du blog

```
nix-shell --run "cabal run site build"
firefox public/index.html
```

