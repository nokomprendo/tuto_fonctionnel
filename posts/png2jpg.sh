for f in `find . -name "thumb*.png"` ; do
  convert "$f" "${f%.png}.jpg"
done
