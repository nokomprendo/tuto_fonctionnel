with import<nixpkgs> {};
stdenv.mkDerivation {
    name = "hellocpp";
    src = ./.;
    buildInputs = [ cmake ];
}

