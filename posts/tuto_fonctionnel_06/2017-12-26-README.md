---
title: Développer en Python avec Nix
---

[vidéo peertube](https://peertube.fr/videos/watch/e0589baa-cb9e-443f-80a8-25ac1af0f5fd) - 
[vidéo youtube](https://www.youtube.com/watch?v=JR7WndlPzVk) - 
[dépôt git](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_06)

# Développer en Python avec Nix

comment développer et installer un script python qui utilise une bibliothèque (numpy)


## Bibliothèque fournie dans les paquets systèmes

- installer les paquets systèmes :

```bash
sudo apt install python3 python3-numpy
```

- installer le script :

```bash
sudo cp hellonumpy.py /usr/bin/
hellonumpy.py
```

- exécuter en local :

```bash
./hellonumpy.py
```

- inconvénients :

    - pas forcément portable sur un autre système d'exploitation
    - nécessite que la bibliothèque soit fournie par le système
    - installation système manuel


## Bibliothèque fournie par Setuptools/Pip/Virtualenv

- installer les outils python nécessaires :

```bash
sudo apt install python3-virtualenv python3-pip
```

- écrire un fichier setup.py 

- installation système :

```bash
sudo python3 setup.py install
hellonumpy.py
```
- installation utilisateur :

```bash
python3 -m venv ~/venv1
source ~/venv1/bin/activate
python3 setup.py install
hellonumpy.py
```

- inconvénients :

    - pas très simple
    - duplication possible dans les venvs
    - mêmes genre de problèmes/solutions pour les autres langages (Javascript, Ruby, Haskell...)


## Avec nix

- écrire un fichier `default.nix`

- installation système :

```bash
nix-env -f . -A hellonumpy
hellonumpy.py
```

- exécution en local :

```bash
nix-shell -A hellonumpy
python3 hellonumpy.py
```

