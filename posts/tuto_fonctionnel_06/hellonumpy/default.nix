{ system ? builtins.currentSystem } :

let
  pkgs = import <nixpkgs> { inherit system; };
in

pkgs.stdenv.mkDerivation {
  hellonumpy = pkgs.python3Packages.buildPythonPackage {
    name = "hellonumpy";
    src = ./.;
    propagatedBuildInputs = [
      (pkgs.python3.buildEnv.override {
       extraLibs = [ pkgs.python3Packages.numpy ];
       })
    ];
  };
}

