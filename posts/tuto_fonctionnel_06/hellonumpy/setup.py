from setuptools import setup, find_packages
setup(
  name = "hellonumpy",
  version = "0.1",
  packages = find_packages(),
  scripts = ["hellonumpy.py"],
  install_requires = ["numpy"],
)

