---
title: Personnaliser un paquet avec Nix
---

[vidéo peertube](https://peertube.fr/videos/watch/3a344dfa-1da8-42c9-bf88-f82cd681c47d) - 
[vidéo youtube](https://www.youtube.com/watch?v=zk08_mG13C8) - 
[dépôt git](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_07)

# Personnaliser un paquet avec Nix

pourquoi et comment changer des options de compilation d'un paquet système


## Exécuter du code OpenCV avec une distribution Linux classique

- en C++ avec les paquets systèmes :

```bash
make
time ./mulimg.out data.csv
```

- en Python avec un environnement virtuel :

```bash
python3 -m venv venv1
source venv1
pip install numpy pandas opencv-python
time ./mulimg.py data.csv
```

- problème : par défaut opencv est souvent compilé sans openblas si bien que la
  version C++ est ici beaucoup plus lente que la version Python :-(


## Avec Nix

- spécifier les paramètres voulus dans le `default.nix`

- en C++ :

```bash
nix-build
time ./result/bin/mulimg.out data.csv
```

- en Python :

```bash
nix-shell --run "time ./mulimg.py data.csv"
```

