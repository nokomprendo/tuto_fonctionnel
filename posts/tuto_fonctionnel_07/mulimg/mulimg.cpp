#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <tuple>

using namespace std;
using Meta = tuple<string, string, string>;

void compute_image(const string & dirname, const string & filename, 
        const string & extname) {
    // read image (grayscale, float64)
    string input_name = dirname + "/" + filename + "." + extname;
    cout << input_name << endl;
    cv::Mat img_in; 
    cv::imread(input_name, cv::IMREAD_GRAYSCALE).convertTo(img_in, CV_64F);
    img_in /= 255.0;

    // mul images
    cv::Mat img_mul = img_in * img_in.t();
    cv::normalize(img_mul, img_mul, 0, 1, cv::NORM_MINMAX);

    // output images
    cv::imwrite("out_" + filename + "_cpp." + extname, img_mul*255.0);
}

int main(int argc, char ** argv) {
    // parameters
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " data_csv\n";
        exit(-1);
    }
    const string data_csv = argv[1];
    cout << data_csv << ' ' << endl;

    // read data file
    vector<Meta> metas;
    ifstream csv_file(data_csv);
    string line;
    while (getline(csv_file, line)) {
        stringstream line_ss(line);
        string dirname, filename, extname;
        line_ss >> dirname >> filename >> extname;
        metas.push_back(Meta{dirname, filename, extname});
    }

    // compute images 
    for (const auto & meta : metas) {
        string dirname, filename, extname;
        tie(dirname, filename, extname) = meta;
        compute_image(dirname, filename, extname);
    }
    return 0;
}


