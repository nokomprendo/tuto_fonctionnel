#!/usr/bin/env python3
import cv2
import csv
import numpy
import pandas
import sys

def compute_image(dirname, filename, extname) :
    # read image (grayscale, float64)
    input_name = dirname + '/' + filename + '.' + extname
    print(input_name)
    img_in = cv2.imread(input_name, cv2.IMREAD_GRAYSCALE)
    img_in = img_in.astype(numpy.float64) / 255.0

    # mul images
    img_mul = numpy.matmul(img_in, cv2.transpose(img_in))
    cv2.normalize(img_mul, img_mul, 0, 1, cv2.NORM_MINMAX)

    # output images
    cv2.imwrite('out_' + filename + '_py.' + extname, img_mul*255.0)

if __name__ == "__main__":
    # parameters
    if len(sys.argv) != 2:
        print('usage:', sys.argv[0], ' data_csv')
        sys.exit(-1)
    data_csv = sys.argv[1]
    print(data_csv)

    # read data file
    metas = pandas.read_csv(data_csv, delimiter=' ', header=None)

    # compute images 
    for _, dirname, filename, extname in metas.itertuples():
        compute_image(dirname, filename, extname)

