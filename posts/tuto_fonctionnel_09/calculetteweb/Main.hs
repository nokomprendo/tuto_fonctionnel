{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import           Lucid
import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import qualified Web.Scotty as S

main = S.scotty 3000 $ do
  S.middleware logStdoutDev
  S.get "/" $ do
    expr <- S.param "expr" `S.rescue` (\_ -> return (""::String))
    S.html $ renderText $ do
      doctype_
      html_ $ body_ $ do
        h1_ "calculette web"
        form_ [action_ "/", method_ "get"] $ do
          input_ [name_ "expr", value_ (T.pack expr)]
          br_ []
          input_ [type_ "submit"]
        p_ $ toHtml $ calculette expr

calculette :: String -> String
calculette "" = ""
calculette str = if null str' then show value else "erreur : " ++ (unwords str')
  where (value, str') = evaluer $ words str

evaluer :: [String] -> (Float, [String])
evaluer [] = (0, ["expression non terminée"])
evaluer (x:xs) = case x of 
  "+" -> (v1+v2, xs2)
  "-" -> (v1-v2, xs2)
  "*" -> (v1*v2, xs2)
  "/" -> (v1/v2, xs2)
  otherwise -> (v, xs) 
  where (v1, xs1) = evaluer xs
        (v2, xs2) = evaluer xs1
        v = read x :: Float

