{
  network.description = "calculetteweb";

  calculetteweb = { pkgs, ... }: 
  let 
    _calculetteweb = import ./default.nix { inherit pkgs; };
  in
  { 
    networking.firewall.allowedTCPPorts = [ 3000 ];

    systemd.services.calculetteweb = { 
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = { 
        ExecStart = "${_calculetteweb}/bin/calculetteweb";
      };
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

