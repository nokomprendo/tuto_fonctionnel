{
  packageOverrides = pkgs: with pkgs; rec {

    _vim = import ./vim.nix { inherit pkgs; };
    _nano = pkgs.nano.override { enableNls = false; };

    myPackages = pkgs.buildEnv {
      name = "myPackages";
      paths = [
        _vim
        _nano
        firefox
        vlc
      ];
    };
  };
}

