#ifndef COUNTER_HPP
#define COUNTER_HPP

class Counter {
  private:
    int _n;
    int _k;
  public:
    Counter(int k);
    int getN() const;
    void countK();
};

#endif // COUNTER_HPP

