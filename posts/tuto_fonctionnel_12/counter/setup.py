from setuptools import setup, Extension

counter_module = Extension('counter',
    include_dirs = ['counter'],
    sources = ['counter/counter.cpp', 'counter/counter_binding.cpp'],
    libraries = ['boost_python'])

setup(name = 'pycounter',
    version = '0.1',
    packages = ['pycounter'],
    ext_modules = [counter_module])

