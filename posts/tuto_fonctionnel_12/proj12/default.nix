{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {} } :

let

  _counter = import ../counter/default.nix {
    inherit pkgs;
  };

  _kounter = import ./kounter.nix {
    inherit (pkgs) fetchgit;
    pyPkgs = pkgs.python3Packages;
  };

in

pkgs.python3Packages.buildPythonPackage {
  name = "proj12";
  src = ./.;
  propagatedBuildInputs = [
    (pkgs.python3.buildEnv.override {
      extraLibs = [ 
        _counter 
        _kounter 
      ];
    })
  ];
}

