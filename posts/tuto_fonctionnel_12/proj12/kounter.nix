{ fetchgit, pyPkgs }:

pyPkgs.buildPythonPackage {

  name = "pykounter";

  #src = /home/nokomprendo/depots/framagit/kounter;

  src = fetchgit {
    url = "https://framagit.org/nokomprendo/kounter";
    rev = "ce07c9dedd12236a9f1873dc637a77a2c67f8987";
    sha256 = "0flbxyq939xwxikg7mzgl4i2j7g3668wcqk6jwg59ly3ldhba9rc";
  };

  buildInputs = [ pyPkgs.boost ];

}

