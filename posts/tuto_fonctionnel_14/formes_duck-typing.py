#!/usr/bin/env python3

class Carre:
    def __init__(self, c):
        self.c = c

    def surface(self):
        return self.c * self.c

class Rectangle:
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def surface(self):
        return self.w * self.h

formes = [Carre(2), Rectangle(2, 3)]

for f in formes:
    # duck typing :
    # appelle la méthode surface de Carre ou de Rectangle, selon le type de f
    print(f.surface())

