#include <iostream>
#include <memory>
#include <vector>
using namespace std;

// type principal
struct Forme {
  // méthode redéfinie dans les sous-types
  virtual double surface() const = 0;
};

struct Carre : Forme {
  double _c;
  Carre(double c) : _c(c) {}
  double surface() const override { return _c * _c; }
};

struct Rectangle : Forme {
  double _w, _h;
  Rectangle(double w, double h) : _w(w), _h(h) {}
  double surface() const override { return _w * _h; }
};

int main() {

  // collection de Carre/Rectangle
  vector<unique_ptr<Forme>> formes;
  formes.push_back(make_unique<Carre>(2));
  formes.push_back(make_unique<Rectangle>(2, 3));

  for (const auto & f : formes) {
    // appelle la méthode correspondant au sous-type
    cout << f->surface() << endl;
  }

  return 0;
}

