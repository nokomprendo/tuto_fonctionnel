#!/usr/bin/env python3

class Carre:
    def __init__(self, c):
        self.c = c

    def surfaceCarre(self):
        return self.c * self.c

class Rectangle:
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def surfaceRectangle(self):
        return self.w * self.h

formes = [Carre(2), Rectangle(2, 3), 42]

for f in formes:
    # typage dynamique :
    # teste si f est de type Rectangle
    if isinstance(f, Rectangle):
        print(f.surfaceRectangle())
    # teste si f est de type Carre
    if isinstance(f, Carre):
        print(f.surfaceCarre())

