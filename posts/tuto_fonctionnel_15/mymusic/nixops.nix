{
  network.description = "mymusic";

  webserver = { config, pkgs, ... }: 
  let
    _mymusic = import ./mymusic.nix { inherit pkgs; };
  in
  {
    networking.firewall.allowedTCPPorts = [ 80 ];

    services.httpd = {
      enable = true;
      adminAddr = "toto@example.com";
      #documentRoot = "/data/public";
      documentRoot = "${_mymusic}/public";
      extraModules = [
        { name = "php7"; path = "${pkgs.php}/modules/libphp7.so"; }
      ];
    };

    services.postgresql = {
      enable = true;
      package = pkgs.postgresql;
      initialScript = "${_mymusic}/private/music.sql";
      enableTCPIP = true;
      authentication = "local all all trust";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };

}

