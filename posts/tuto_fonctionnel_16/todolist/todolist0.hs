{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Clay as C
import Control.Monad.Trans (liftIO)
import Data.Aeson (ToJSON)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)
import Web.Scotty (middleware, scotty, get, json, html, param, post)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Lucid

-- Model

{- SQL
data Task = Task
  { taskId :: Int
  , taskName :: L.Text
  } deriving (Show, Generic)

instance FromRow Task where
  fromRow = Task <$> field <*> field

dbName :: String
dbName = "todolist.db"

selectTasks :: IO [Task]
selectTasks = 
  SQL.withConnection dbName (\c -> SQL.query_ c "SELECT * FROM task")

insertTask :: L.Text -> IO ()
insertTask theName = SQL.withConnection dbName 
  (\c -> SQL.execute c "INSERT INTO task (name) VALUES (?)" (SQL.Only theName))

deleteTask :: Int -> IO ()
deleteTask theId = SQL.withConnection dbName 
  (\c -> SQL.execute c "DELETE FROM task WHERE id = ?" (SQL.Only theId))
-}

{- JSON
instance ToJSON Task
-}

-- View

{- CSS
bodyCss :: C.Css
bodyCss = C.body C.? do
  C.backgroundColor  C.beige

divCss :: C.Css
divCss = C.div C.? do
  C.border           C.solid (C.px 1) C.black
  C.backgroundColor  C.azure
-}

{- HTML
renderHome :: [Task] -> L.Text
renderHome theTasks = renderText $ do
  doctype_
  html_ $ do
    {- CSS
    header_ $ style_ $ L.toStrict $ C.render $ do
      bodyCss
      divCss
    -}
    body_ $ do
      h1_ "TODO list"
      div_ $ ul_ $ mapM_ formatTask theTasks
      form_ [action_ "/add", method_ "post"] $ do
        p_ $ do
          "New task: " 
          input_ [name_ "task_name"] 
          input_ [type_ "submit"]
      p_ $ a_ [href_ "tasks"] "Get JSON data"
    where formatTask :: Task -> Lucid.Html ()
          formatTask (Task tId tName) = 
            li_ $ do
              toHtml tName
              " - "
              a_ [href_ (T.concat ["del?task_id=", T.pack $ show tId])] "delete"
  -}

{- SQL
renderHome :: [Task] -> L.Text
renderHome = L.pack . show
-}

-- Main

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  middleware simpleCors

  get "/" $ html "<html><body><a href='page2'>page2</a></body></html>"

  get "/page2" $ html "hello"

  {- SQL
  get "/" $ do
    myTasks <- liftIO selectTasks
    html $ renderHome myTasks

  post "/add" $ do
    myName <- param "task_name"
    liftIO $ insertTask myName
    myTasks <- liftIO selectTasks
    html $ renderHome myTasks

  get "/del" $ do
    myId <- param "task_id"
    liftIO $ deleteTask (read myId :: Int)
    myTasks <- liftIO selectTasks
    html $ renderHome myTasks
  -}

  {- JSON
  get "/tasks" $ liftIO selectTasks >>= json
  -}

