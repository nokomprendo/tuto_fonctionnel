with import <nixpkgs> {};

let
  _myhellolib = import ./myhellolib.nix { inherit stdenv fetchurl cmake; };
in

stdenv.mkDerivation {
  name = "test_myhellolib1";
  src = ./.;
  buildInputs = [ _myhellolib ];
  buildPhase = "g++ -o test_myhellolib1 test_myhellolib.cpp -l myhellolib";
  installPhase = ''
      mkdir -p $out/bin
      cp test_myhellolib1 $out/bin/
  '';
}

