{ stdenv, fetchurl, cmake }:

stdenv.mkDerivation rec {
  name = "myhellolib";
  version = "0.1";
  src = fetchurl {
    url = "https://framagit.org/nokomprendo/${name}/-/archive/v${version}/${name}-v${version}.tar.gz";
    sha256 = "1zyvvf3px69wlzvxrgcgnrkkqwyphpqlqrwxk72cs2vdfd6sh9gn";
  };
  buildInputs = [ cmake ];
}

