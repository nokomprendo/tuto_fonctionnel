with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "mydisplay";
  src = ./.;
  buildInputs = [ cmake pkgconfig opencv ];

  #configurePhase = ''
  #  mkdir build
  #  cd build
  #  cmake -DCMAKE_INSTALL_PREFIX=$out ..
  #'';

  #buildPhase = ''
  #  make
  #'';

  #installPhase = ''
  #  make install
  #'';
}

#with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/1bc5bf4beb759e563ffc7a8a3067f10a00b45a7d.tar.gz") {};

#let
#  _stdenv = overrideCC stdenv gcc5;
#
#  _opencv_gtk = opencv.override { 
#    stdenv = _stdenv;
#    cmake = cmake_2_8; 
#    enableGtk2 = true; 
#  };
#
#  _opencv_gtk_att = _opencv_gtk.overrideDerivation (attrs: {
#    cmakeFlags = [ attrs.cmakeFlags "-DENABLE_PRECOMPILED_HEADERS=OFF" ];
#  });
#in

