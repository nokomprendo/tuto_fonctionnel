{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  pkg = pkgs.stdenv.mkDerivation {
    name = "animals-elm";
    src = ./.;
    buildInputs = [ pkgs.elmPackages.elm-make ];
    buildPhase = ''
      export HOME=`pwd`
      elm-make Main.elm --yes --output tmp.js
      ${pkgs.closurecompiler}/bin/closure-compiler tmp.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars > all.js

    '';
    installPhase = ''
      mkdir -p $out
      cp index.html all.js $out/
    '';
  };

  env = with pkgs; stdenv.mkDerivation {
    name = "animals-elm-env";
    src = ./.;
    buildInputs = with elmPackages; [
      elm-make
    ];
    installPhase = "";
    shellHook = ''
      alias mymake="elm-make Main.elm --yes --output all.js"
    '';
  };

in

if pkgs.lib.inNixShell then env else pkg

