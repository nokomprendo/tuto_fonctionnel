{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (FromJSON, decodeStrict)
import Data.Maybe (fromMaybe)
import Data.Monoid ((<>))
import Data.Text (Text)
import GHC.Generics (Generic)
import JavaScript.Web.XMLHttpRequest (Request(..), RequestData(..), Method(..), contents, xhrByteString)
import Miso
import Miso.String (MisoString, toMisoString, fromMisoString, pack)

main :: IO ()
main = startApp App 
    { model = Model []
    , update = updateModel
    , view = viewModel
    , subs = []
    , events = defaultEvents
    , initialAction = GetAnimals ""
    , mountPoint = Nothing
    }

-- Model

data Animal = Animal 
    { animalType :: Text
    , animalImage :: Text
    } deriving (Eq, Generic, Show)

instance FromJSON Animal

data Model = Model { modelAnimals :: [Animal] } deriving (Eq, Show)

-- Controler

data Action 
    = GetAnimals MisoString 
    | SetAnimals [Animal]
    | NoOp 
    deriving (Show, Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel (GetAnimals str) m = m <# (SetAnimals <$> queryAnimals str)
updateModel (SetAnimals animals) m = noEff m { modelAnimals = animals }
updateModel NoOp m = noEff m

queryAnimals :: MisoString -> IO [Animal]
queryAnimals str = do
    let uri = pack $ "http://localhost:3000/api/animals/" ++ fromMisoString str
        req = Request GET uri Nothing [] False NoData
    Just cont <- contents <$> xhrByteString req
    return $ fromMaybe [] $ decodeStrict cont

-- View

viewModel :: Model -> View Action
viewModel (Model animals) = 
    span_ []
        [ h1_ [] [ text "Animals (Miso)" ]
        , p_ [] [ input_ [ onInput GetAnimals ] ]
        , span_ [] $ map fmtAnimal animals
        ]

fmtAnimal :: Animal -> View Action
fmtAnimal animal = 
    div_ [] 
        [ p_ [] [ text $ toMisoString $ animalType animal ]
        , img_ [ src_ $ toMisoString $ "http://localhost:3000/img/" <> animalImage animal
               , width_ "320"
               , height_ "240"
               ]
        ]

