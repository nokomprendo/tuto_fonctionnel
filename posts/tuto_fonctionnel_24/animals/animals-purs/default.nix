{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/10b979ff213d7e63a6e072d0433a21687e311483.tar.gz") {} }:

let

  pkg = pkgs.stdenv.mkDerivation {
    name = "animals-purs";
    src = ./.;
    buildInputs = with pkgs; [
      closurecompiler
      cacert
      git
      nodejs-8_x
      nodePackages.bower
      nodePackages.pulp
      purescript
    ];
    buildPhase = ''
      export HOME=`pwd`
      bower install
      pulp build --to tmp.js
      ${pkgs.closurecompiler}/bin/closure-compiler tmp.js > all.js
    '';
    installPhase = ''
      mkdir -p $out
      cp index.html all.js $out/
    '';
  };

  env = pkgs.stdenv.mkDerivation {
    name = "animals-purs-env";
    src = ./.;
    buildInputs = with pkgs; [
      git
      nodejs-8_x
      nodePackages.bower
      nodePackages.pulp
      purescript
    ];
    installPhase = "";
    shellHook = ''
      alias myclean="rm -rf bower_components node_modules .pulp-cache output all.js"
    '';
  };

in

if pkgs.lib.inNixShell then env else pkg


