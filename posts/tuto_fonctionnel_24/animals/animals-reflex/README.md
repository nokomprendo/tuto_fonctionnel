# animals-reflex

Ne pas oublier de lancer le serveur (animals-scotty).


## avec nix-build

```
nix-build
firefox result/index.html
```


## avec nix-shell


```
nix-shell
```

- la première fois :

```
cabal new-build all
ln -s dist-newstyle/build/x86_64-linux/ghcjs-0.2.1/animals-reflex-0.1.0.0/c/animals-reflex/build/animals-reflex/animals-reflex.jsexe/all.js .
firefox index.html
```

- les fois suivantes :

```
cabal new-build all
```

