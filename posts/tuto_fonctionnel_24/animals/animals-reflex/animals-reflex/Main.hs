{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Data.Aeson (FromJSON)
import Data.Default (def)
import Data.Maybe (fromJust)
import Data.Monoid ((<>))
import Data.Text (Text)
import GHC.Generics (Generic)
import Prelude
import Reflex (holdDyn)
import Reflex.Dom 
import Reflex.Dom.Xhr (decodeXhrResponse, performRequestAsync, XhrRequest(..))

main :: IO ()
main = mainWidget ui

-- Model

data Animal = Animal 
    { animalType :: Text
    , animalImage :: Text
    } deriving (Eq, Generic, Show)

instance FromJSON Animal

-- View / Controler

ui :: MonadWidget t m => m ()
ui = do
    el "h1" $ text "Animals (Reflex)"
    myInput <- el "p" $ textInput def
    evStart <- getPostBuild
    let evs = [ () <$ _textInput_input myInput , evStart ]
    let evCode = tagPromptlyDyn (value myInput) (leftmost evs)
    evResponse <- performRequestAsync $ queryAnimals <$> evCode
    let evResult = fromJust . decodeXhrResponse <$> evResponse
    dynAnimals :: (Dynamic t [Animal]) <- holdDyn [] evResult 
    _ <- el "span" $ simpleList dynAnimals displayAnimal
    return ()

queryAnimals :: Text -> XhrRequest ()
queryAnimals code = XhrRequest "GET" ("http://localhost:3000/api/animals/" <> code) def

displayAnimal :: MonadWidget t m => Dynamic t Animal -> m ()
displayAnimal dynAnimal = do
    let imgSrc = (<>) "http://localhost:3000/img/" . animalImage <$> dynAnimal
    let imgAttrs0 = ("width" =: "320") <> ("height" =: "240")
    let imgAttrs = ((<>) imgAttrs0) . (=:) "src" <$> imgSrc
    el "div" $ do
        el "p" $ dynText $ animalType <$> dynAnimal
        elDynAttr "img" imgAttrs $ dynText imgSrc

