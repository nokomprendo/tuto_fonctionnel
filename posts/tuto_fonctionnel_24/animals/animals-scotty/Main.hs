{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (ToJSON)
import Data.Maybe (fromMaybe)
import Data.Text.Lazy (isPrefixOf, Text, toLower)
import GHC.Generics (Generic)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Text.Read (readMaybe)
import Web.Scotty (get, json, middleware, param, scotty)

data Animal = Animal 
    { animalType :: Text
    , animalImage :: Text
    } deriving (Generic, Show)

instance ToJSON Animal

myAnimals :: [Animal]
myAnimals = 
    [ Animal "dauphin" "dolphin-marine-mammals-water-sea-64219.jpeg"
    , Animal "chien" "night-garden-yellow-animal.jpg"
    , Animal "chouette" "owl.jpeg"
    , Animal "panda" "pexels-photo-1123765.jpeg"
    , Animal "chouette" "pexels-photo-105810.jpeg"
    , Animal "panda" "pexels-photo-146087.jpeg"
    , Animal "goeland" "pexels-photo-213697.jpeg"
    , Animal "cheval" "pexels-photo-614505.jpeg"
    , Animal "goeland" "pexels-photo-213191.jpeg"
    , Animal "cheval" "pexels-photo-635499.jpeg"
    , Animal "chat" "pexels-photo-669015.jpeg"
    , Animal "chien" "pexels-photo-733416.jpeg"
    , Animal "chat" "pexels-photo-747795.jpeg"
    , Animal "dauphin" "pexels-photo-994473.jpeg"
    ]

findAnimals :: Text -> [Animal]
findAnimals t = filter (isPrefixOf t' . animalType') myAnimals
    where t' = toLower t
          animalType' = toLower . animalType

main :: IO ()
main = do
    portMayStr <- lookupEnv "PORT"
    let port = fromMaybe 3000 (portMayStr >>= readMaybe)
    scotty port $ do
        middleware logStdoutDev
        middleware simpleCors
        get "/api/animals" $ json myAnimals
        get "/api/animals/:type" $ param "type" >>= json . findAnimals
        middleware $ staticPolicy $ addBase "static"

