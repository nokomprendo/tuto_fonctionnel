{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  app = pkgs.haskellPackages.callCabal2nix "animals-scotty" ./. {};

  pkg = pkgs.stdenv.mkDerivation {
    name = "animals-scotty";
    src = ./.;
    buildInputs = [ app ];
    installPhase = ''
      mkdir $out
      cp ${app}/bin/* $out/
      cp -R static $out/
    '';
  };

  env = app.env;

in

if pkgs.lib.inNixShell then env else pkg


