{-# language DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (forkIO)
import Control.Monad (forever, unless, liftM)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (fromStrict)
import Data.IORef (atomicWriteIORef, IORef, newIORef, readIORef)
import qualified Web.Scotty as SC

import OpenCV
import OpenCV.VideoIO.Types

openCam :: IO (Maybe VideoCapture)
openCam = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> return Nothing
        True -> videoCaptureSetD cap VideoCapPropFps 5 >> (return $ Just cap)

captureCam :: VideoCapture -> IO (Maybe (Mat ('S ['D, 'D]) 'D 'D))
captureCam cap = videoCaptureGrab cap >> videoCaptureRetrieve cap 

-- imgToPng :: Mat ('S ['D, 'D]) 'D 'D -> ByteString
-- imgToPng = exceptError . imencode (OutputPng defaultPngParams)

main :: IO ()
main = do
    capMaybe <- openCam
    case capMaybe of
        Nothing -> putStrLn "couldn't open device"
        Just cap -> do
            withWindow "webcamer" (loopCam cap)

            -- runServer 3042 cap

            -- Just png0 <- liftM imgToPng <$> captureCam cap
            -- pngRef <- newIORef png0
            -- _ <- forkIO $ runCam cap pngRef
            -- runServer 3042 pngRef

loopCam :: VideoCapture -> Window -> IO ()
loopCam cap window = do
    imgMaybe <- captureCam cap
    case imgMaybe of
        Nothing -> return ()
        Just img -> do
            imshow window img
            key <- waitKey 20
            unless (key == 27) $ loopCam cap window

{-
runServer :: Int -> VideoCapture -> IO ()
runServer port cap = SC.scotty port $ do
    SC.get "/" $ SC.file "index.html"
    SC.get "/out.png" $ do
        SC.setHeader "Content-Type" "image/png"
        imgMaybe <- SC.liftAndCatchIO $ liftM imgToPng <$> captureCam cap
        case imgMaybe of
            Nothing -> return ()
            Just img -> SC.raw $ fromStrict img

-- runServer :: Int -> IORef ByteString -> IO ()
-- runServer port pngRef = SC.scotty port $ do
        -- img <- SC.liftAndCatchIO (readIORef pngRef) 
        -- SC.raw $ fromStrict img
-}

{-
runCam :: VideoCapture -> IORef ByteString -> IO ()
runCam cap pngRef = forever $ do
    imgMaybe <- liftM imgToPng <$> captureCam cap
    mapM_ (atomicWriteIORef pngRef) imgMaybe
-}

