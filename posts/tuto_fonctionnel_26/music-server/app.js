"use strict";

const myMusics = [
    { musicSong: "Paranoid android", musicBand: "Radiohead", musicYear: 1997 },
    { musicSong: "Just", musicBand: "Radiohead", musicYear: 1995 },
    { musicSong: "Take the power back", musicBand: "Rage against the machine", musicYear: 1991 },
    { musicSong: "How I could just kill a man", musicBand: "Rage against the machine", musicYear: 2000 },
    { musicSong: "La porte bonheur", musicBand: "Ibrahim Maalouf", musicYear: 2014 }
];

const express = require("express");
const app = express();

app.get("/", function (req, res) {
    res.json(myMusics);
});

const port = 3000;

const server = app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});

