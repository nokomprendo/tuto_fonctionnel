# get and configure an image for building our app
FROM debian:stretch-slim as builder
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
		ca-cacert \
		cmake \
		build-essential \
    libboost-all-dev \
    libssl-dev \
    wget \
    zlib1g-dev

# get and build wt4
WORKDIR /root
RUN wget https://github.com/emweb/wt/archive/4.0.4.tar.gz
RUN tar zxf 4.0.4.tar.gz
WORKDIR /root/wt-4.0.4/build
RUN cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF -DBUILD_EXAMPLES=OFF -DSHARED_LIBS=OFF ..
RUN make -j2 install

# build our app (static linking)
WORKDIR /root/myrepeat
ADD . /root/myrepeat
RUN g++ -static -O2 -o myrepeat myrepeat.cpp -pthread -lwthttp -lwt \
        -lboost_system -lboost_thread -lboost_filesystem \
        -lboost_signals -lboost_program_options \
        -lz -lssl -lcrypto -ldl

# create the final image
FROM scratch
COPY --from=builder /root/myrepeat/myrepeat /
ENTRYPOINT ["/myrepeat"]
CMD [""]

