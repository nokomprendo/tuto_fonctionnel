# myrepeat_v3

## build & run locally

```
nix-build
./result/bin/myrepeat --docroot . --http-address 0.0.0.0 --http-port 3000
```

## build & run a docker image

```
nix-build docker.nix && docker load < result
docker run --rm -it -e PORT=3000 -p 3000:3000 myrepeat:v3
```

image size: 579MB
 

## deploy on heroku

```
heroku container:login
heroku create myrepeat
docker tag myrepeat:v3 registry.heroku.com/myrepeat/web
docker push registry.heroku.com/myrepeat/web
heroku container:release web --app myrepeat
```

