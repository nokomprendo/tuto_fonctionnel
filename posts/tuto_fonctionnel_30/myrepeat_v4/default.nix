{ pkgs ? import <nixpkgs> {}, wt ? pkgs.wt }: 

pkgs.stdenv.mkDerivation {
  name = "myrepeat";
  src = ./.;
  buildInputs = [ wt ];
  buildPhase = "g++ -O2 -o myrepeat myrepeat.cpp -lwthttp -lwt";
  installPhase = ''
    mkdir -p $out/bin
    cp myrepeat $out/bin/
  '';
}

