{ stdenv, fetchFromGitHub, cmake, boost, openssl, zlib }:

stdenv.mkDerivation {

  name = "wt";

  src = fetchFromGitHub {
    owner = "emweb";
    repo = "wt";
    rev = "4.0.4";
    sha256 = "17kq9fxc0xqx7q7kyryiph3mg0d3hnd3jw0rl55zvzfsdd71220w";
  };

  enableParallelBuilding = true;

  buildInputs = [
    cmake
    boost
    openssl 
    zlib
  ];

  cmakeFlags = [
    "-DCMAKE_BUILD_TYPE=Release"
    "-DBUILD_TESTS=OFF"
    "-DBUILD_EXAMPLES=OFF"
  ];

}

