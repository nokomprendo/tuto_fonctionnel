# Frameworks used in the "Animals" projects

... for implementing the HTTP server, the HTML generation and the SQL database
access.

| project | web framework | HTML generator | SQL interface |
|---|---|---|---|
| [animals-cppcms](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms) | [cppcms](http://cppcms.com/wikipp/en/page/main/) (web framework) | cppcms (templating system) | cppcms (SQL connector) |
| [animals-cpprestsdk](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cpprestsdk) | [cpprestsdk](https://github.com/Microsoft/cpprestsdk) (asynchronous networking framework) | [ctml](https://github.com/tinfoilboy/CTML) (html document generator) | [sqlite_orm](https://github.com/fnc12/sqlite_orm) (ORM) |
| [animals-crow](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-crow) | http: [crow](https://github.com/ipkn/crow) (lightweight web framework) | crow (templating system) | [sqlpp11](https://github.com/rbock/sqlpp11) (ORM) |
| [animals-nodejs](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-nodejs) (Javascript/Node.js) | [express](https://expressjs.com/) (asynchronous lightweight web framework) | [pug](https://pugjs.org) (document generator) | [better-sqlite3](https://github.com/JoshuaWise/better-sqlite3) (SQL connector) |
| [animals-pistache](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-pistache) | [pistache](http://pistache.io/) (asynchronous lightweight web framework) | [kainjow mustache](https://github.com/kainjow/Mustache) (templating system) | [sqlite_modern_cpp](https://github.com/SqliteModernCpp/sqlite_modern_cpp) (SQL connector) |
| [animals-scotty](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-scotty) (Haskell) | [scotty](http://hackage.haskell.org/package/scotty) (lightweight web framework) | [lucid](https://hackage.haskell.org/package/lucid) and [clay](https://hackage.haskell.org/package/clay) (document generators) | [sqlite-simple](https://hackage.haskell.org/package/sqlite-simple) (SQL connector) |
| [animals-silicon](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-silicon) | [silicon](http://siliconframework.org/) (lightweight web framework) | none | silicon (SQL connector) |
| [animals-tntnet](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-tntnet) | [tntnet](http://www.tntnet.org/tntnet.html) (template-based web framework) | tntnet (templating system) | tntnet (SQL connector) |
| [animals-wt](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-wt) | [wt](https://www.webtoolkit.eu/wt) (web GUI framework) | wt (widget system + templates) | wt (ORM) |

