with import <nixpkgs> {};

mkShell {
  buildInputs = [
    cppcms
    cppdb
    gnumake
    sqlite
  ];
}

