#ifndef CONTENT_H
#define CONTENT_H

#include "Animal.hpp"

#include <cppcms/view.h>
#include <cppcms/form.h>

// define how to exchange data between the C++ code and the templates
namespace content  {

  // content for MasterView and AboutView
  struct MasterContent : cppcms::base_content {
    std::string title;  // the "title" parameter in master.tmpl
  };

  // datatype for the form in HomeView
  struct InfoForm : cppcms::form {
    cppcms::widgets::text myquery; 
    InfoForm() {
      add(myquery);
    }
  };

  // content for HomeView 
  // inherits from MasterContent, because HomeView inherits from MasterView
  struct HomeContent : MasterContent {
    std::vector<Animal> animals;  // the "animals" parameter in home.tmpl
    InfoForm info;  // the "info" parameter in home.tmpl
  };

}

#endif

