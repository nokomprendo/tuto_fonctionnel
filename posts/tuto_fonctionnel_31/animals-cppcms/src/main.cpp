#include <cppcms/application.h>
#include <cppcms/applications_pool.h>
#include <cppcms/service.h>
#include <cppcms/url_dispatcher.h>
#include <cppcms/url_mapper.h>

#include "content.h"
#include "Animal.hpp"

#include <fstream>
#include <iostream>

using namespace std;

// main application
class App : public cppcms::application {

  public:
    App(cppcms::service &srv) : cppcms::application(srv) {
      // about page
      dispatcher().assign("/about", &App::about, this);
      mapper().assign("about","/about");
      // home page
      dispatcher().assign("", &App::home, this);
      mapper().assign("");
      // images
      dispatcher().assign("/img/([a-z_0-9_\\-]+\\.jpg)", &App::serveJpg, this, 1);
      // root url
      mapper().root("/animals");
    }

  private:
    void about() {
      // AboutView inherits from MasterView and uses the same content type (MasterContent)
      content::MasterContent c;
      c.title = "About (Cppcms)";
      // render the AboutView template
      render("AboutView", c);
    }

    void home()  {
      // HomeView inherits from MasterView and uses its own content type 
      // (HomeContent, that inherits from MasterContent)
      content::HomeContent c;
      // data defined in MasterContent 
      c.title = "Animals (Cppcms)";
      // data defined in HomeContent
      c.info.load(context());
      c.animals = getAnimals(c.info.myquery.value());
      // render the HomeView template
      render("HomeView", c);
    }

    void serveJpg(string filename)  {
      // open and send the image file
      ifstream ifs("img/" + filename);
      if (ifs) {
        response().content_type("image/jpeg");
        response().out() << ifs.rdbuf();
      }
      else {
        response().status(404);
      }
    }
};

// create and run the application
int main(int argc, char ** argv) {
  try {
    cppcms::service srv(argc, argv);
    srv.applications_pool().mount(cppcms::applications_factory<App>());
    srv.run();
  }
  catch(exception const & e) {
    cerr << e.what() << endl;
  }
  return 0;
}

