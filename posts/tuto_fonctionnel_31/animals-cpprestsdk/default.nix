with import <nixpkgs> {};

let
  _cpprestsdk = callPackage ./nix/cpprestsdk.nix {};
  _ctml = callPackage ./nix/ctml.nix {};
  _sqlite_orm = callPackage ./nix/sqlite_orm.nix {};
in

stdenv.mkDerivation {
  name = "animals-cpprestsdk";
  src = ./.;

  buildInputs = [
    boost
    _cpprestsdk
    _ctml
    openssl
    sqlite
    _sqlite_orm
  ];

  buildPhase = '' 
    g++ -O2 -o animals-cpprestsdk src/*.cpp -lcpprest -lboost_system -lssl -lsqlite3
    sqlite3 animals.db < animals.sql
  '';

  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/static
    cp animals-cpprestsdk $out/bin/
    cp $src/static/* $out/static/
    cp animals.db $out/
  '';
}


