{ stdenv, fetchFromGitHub, cmake, boost, openssl, websocketpp, zlib }:

stdenv.mkDerivation {

  name = "cpprestsdk";

  src = fetchFromGitHub {
    owner = "Microsoft";
    repo = "cpprestsdk";
    rev = "204a52610234ac5180e80a6883b62c0ad085f51e";
    sha256 = "0mj2m6n889zdhwxdx24ljxfqryivvn3w9vzs94ppzcx5apa5jb3w";
  };

  enableParallelBuilding = true;

  buildInputs = [ boost cmake openssl websocketpp zlib ];
}

