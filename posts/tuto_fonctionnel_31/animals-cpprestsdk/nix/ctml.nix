{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {

  name = "ctml";

  src = fetchFromGitHub {
    owner = "tinfoilboy";
    repo = "CTML";
    rev = "0eb16c35c591f990db32d41c58bce71d41817b8c";
    sha256 = "1pf7x81iww7cw85vf5rgfxsplx61izmamz8djlll6n6ifvdxkk0f";
  };

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/include/CTML
    cp $src/CTML/*.h $out/include/CTML/
  '';
}

