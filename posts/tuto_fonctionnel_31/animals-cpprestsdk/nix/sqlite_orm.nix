{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {

  name = "sqlite_orm";

  src = fetchFromGitHub {
    owner = "fnc12";
    repo = "sqlite_orm";
    rev = "8ee3d476dacd9374b22c5711af076e05150a0985";
    sha256 = "0iwz2i8bm60dfy0y9c1qb5z10293xxcyqw2p6n22vbjizczhmc66";
  };

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/include
    cp -r include/* $out/include/
  '';
}

