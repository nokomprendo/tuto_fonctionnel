#ifndef VIEW_HPP
#define VIEW_HPP

#include "Animal.hpp"

// render the about page to HTML
std::string renderAbout();

// render the home page to HTML
std::string renderHome(
    const std::string & myquery, 
    const std::vector<Animal> & animals);

#endif

