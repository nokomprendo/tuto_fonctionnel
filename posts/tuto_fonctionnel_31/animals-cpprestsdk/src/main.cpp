// https://microsoft.github.io/cpprestsdk/index.html

#include "View.hpp"

#include <cpprest/filestream.h>
#include <cpprest/http_listener.h>

namespace listener = web::http::experimental::listener;
using status_codes = web::http::status_codes;

using namespace std;
using namespace std::placeholders;

// define server app
class App : public listener::http_listener {
  private:
    // define a handler function for the HTTP GET method
    void handleGet(web::http::http_request req) {
      const auto & uri = req.relative_uri();
      const auto & path = uri.path();
      const auto splitPath = web::uri::split_path(path);

      // route the about page
      if (path == "/about") {
        string html = renderAbout();
        req.reply(status_codes::OK, html, "text/html");
      }
      // route the home page
      else if (path == "/") {
        // get the "myquery" parameter
        const auto splitQuery = web::uri::split_query(uri.query());
        const auto it = splitQuery.find("myquery");
        const string myquery = it != end(splitQuery) ? it->second : "";
        // build and send the page
        vector<Animal> animals = getAnimals(myquery);
        string html = renderHome(myquery, animals);
        req.reply(status_codes::OK, html, "text/html");
      }
      // serve static files
      else if (splitPath.size() == 2 and splitPath[0] == "static") {
        // get the requested filename
        const string image = "static/" + splitPath[1];
        // load the file (asynchronously)
        concurrency::streams::fstream::open_istream(image, ios::in)
          .then([=](concurrency::streams::istream is) {
              // send the file when ready
              req.reply(status_codes::OK, is, "image/jpeg")
              .then([image](pplx::task<void> t) {
                  // handle error in sending
                  try { t.get(); }
                  catch(...) {} }); })
          .then([=](pplx::task<void>t) {
              // handle error in loading
              try { t.get(); }
              catch(...) { req.reply(status_codes::InternalError); }});
      }
      else {
        req.reply(status_codes::NotFound);
      }
    }

  public:
    App(string url) : listener::http_listener(url) {
      support(web::http::methods::GET, bind(&App::handleGet, this, _1));
    }
};

// run server app on port 3000
int main() {
  const string address = "http://0.0.0.0:3000";
  App app(address);
  app.open().wait();
  cout << "Listening for requests at: " << address << endl;
  string line;
  getline(cin, line);
  app.close().wait();
  return 0;
}

