{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {
  name = "crow";

  src = fetchFromGitHub {
    owner = "ipkn";
    repo = "crow";
    rev = "49edf898a5b9a39a0d69072cc2434c4f23692908";
    sha256 = "1qkyjc9yc2ak960wkcwp5kvxm4m4xqw0dvknsc9n292xcjlnga5v";
  };

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/include
    cp -R include/* $out/include/
  '';

}

