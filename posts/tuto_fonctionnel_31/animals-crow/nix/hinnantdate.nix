{ stdenv, fetchFromGitHub, cmake, curl }:

stdenv.mkDerivation {
  name = "hinnantdate";

  src = fetchFromGitHub {
    owner = "HowardHinnant";
    repo = "date";
    rev = "e7e1482087f58913b80a20b04d5c58d9d6d90155";
    sha256 = "1761y28zc6a5adnp09pkjjgir2i0b065gdy0jwwqwi0q3g1zp0h5";
  };

  enableParallelBuilding = true;

  buildInputs = [
    cmake
    curl
  ];
}

