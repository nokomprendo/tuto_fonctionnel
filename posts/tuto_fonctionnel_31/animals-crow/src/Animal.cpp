#include "Animal.hpp"
#include "AnimalSql.h"

#include <sqlpp11/sqlite3/sqlite3.h>
#include <sqlpp11/sqlpp11.h>

using namespace std;

namespace sql = sqlpp::sqlite3;

vector<Animal> getAnimals(const string & myquery) {
  vector<Animal> animals;

  // open database
  sql::connection_config config;
  config.path_to_database = "animals.db";
  config.flags = SQLITE_OPEN_READWRITE;
  sql::connection db(config);

  // fetch data
  AnimalSql::Animals animalsSql;
  auto s = dynamic_select(db, animalsSql.name, animalsSql.image)
    .from(animalsSql)
    .dynamic_where();
  s.where.add(animalsSql.name.like(myquery+"%"));
  for (const auto & row : db(s))
    animals.push_back({row.name, row.image});
  return animals;
}


