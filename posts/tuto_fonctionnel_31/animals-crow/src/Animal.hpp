#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <string>
#include <vector>

// Animal datatype
struct Animal {
  std::string name;
  std::string image;
};

// query database (select animals whose name begins with myquery)
std::vector<Animal> getAnimals(const std::string & myquery);

#endif

