#include "Animal.hpp"
#include "View.hpp"

#include <crow.h>
#include <fstream>
#include <map>
#include <sstream>
#include <string>

using namespace std;

// create and run the main application
int main() {
    crow::SimpleApp app;

    // about page
    CROW_ROUTE(app, "/about") (renderAbout);

    // home page
    CROW_ROUTE(app, "/")
      ([](const crow::request & req){
         auto pMyquery = req.url_params.get("myquery");
         string myquery = pMyquery == nullptr ? "" : pMyquery;
         const vector<Animal> animals = getAnimals(myquery);
         return renderHome(myquery, animals);
       });

    // serve static files (JPG)
    CROW_ROUTE(app, "/static/<string>") 
        ([](string filename){
            ostringstream oss;
            ifstream ifs("static/"+filename);
            if (ifs and oss << ifs.rdbuf()) {
                crow::response r(oss.str());
                r.add_header("Content-Type", "image/jpeg");
                return r;
            }
            else
                return crow::response(404);
        });

    // run the app
    app.port(3000).multithreaded().run();
}

