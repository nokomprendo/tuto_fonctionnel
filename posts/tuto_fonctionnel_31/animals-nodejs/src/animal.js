"use strict";

const db = require("better-sqlite3")("animals.db");

// query database (select animals whose name begins with myquery)
exports.getAnimals = myquery =>
    db.prepare("SELECT name,image FROM animals WHERE name LIKE ?||'%'").all(myquery);

