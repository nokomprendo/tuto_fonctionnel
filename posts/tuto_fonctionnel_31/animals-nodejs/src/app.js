"use strict";

const port = 3000;

const view = require("./view.js")
const animal = require("./animal.js")

const express = require("express");
const app = express();

// serve the about page
app.get("/about", function (request, response) {
    const html = view.renderAbout();
    response.send(html);
});

// serve the home page (and filter the animals using the myquery parameter)
app.get("/", function (request, response) {
    const myquery = request.query.myquery ? request.query.myquery : ""
    const animals = animal.getAnimals(myquery);
    const html = view.renderHome({myquery, animals});
    response.send(html);
});

// serve static files (located in the "static" directory)
app.use("/static", express.static("./static"));

// run a server on port 3000
app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});

// async examples

function sendInPromise (response, message) { 
    return new Promise( function(resolve, reject) {
        if (message !== "") {
            response.send(message);
            resolve("message sent");
        }
        else
            reject("message not sent");
    });
}

app.get("/example1", function (request, response) {
    const html = "<h1>example1 (then)</h1>";

    sendInPromise(response, html)
        .then( txt => console.log("example1_a OK:", txt) )
        .then( _ => sendInPromise(response, "") )
        .then( txt => console.log("example1_b OK:", txt) )
        .catch( err => console.log("example1 KO:", err) );

    console.log("end of example1");
});

app.get("/example2", function (request, response) {
    const html = "<h1>example2 (async/await)</h1>";

    async function asyncSend (response, message) {
        const wait_a = sendInPromise(response, message);
        console.log("example2_a OK:", await wait_a);
        const wait_b = sendInPromise(response, "");
        console.log("example2_b OK:", await wait_b);
    }
    asyncSend(response, html)
        .catch( err => console.log("example2 KO:", err) );

    console.log("end of example2");
});

