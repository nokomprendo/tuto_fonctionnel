{ stdenv, fetchFromGitHub, cmake }:

stdenv.mkDerivation {
  name = "pistache";
  src = fetchFromGitHub {
    owner = "oktal";
    repo = "pistache";
    rev = "673cf423261dd3101b9bac81f9141ecaf7a428b1";
    sha256 = "1hwii677pgdch97fkbk78rkdrckwji4i3632dqdn8fjq36dbm19w";
  };
  buildInputs = [ cmake ];
}

