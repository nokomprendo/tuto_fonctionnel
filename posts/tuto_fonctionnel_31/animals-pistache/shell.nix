with import <nixpkgs> {};

let
  _pistache = callPackage ./nix/pistache.nix {};

  _mustache = callPackage ./nix/mustache.nix {};

  _sqlite_modern_cpp = callPackage ./nix/sqlite_modern_cpp.nix {};

in

mkShell {
  buildInputs = [
    gnumake
    _pistache
    _mustache
    sqlite
    _sqlite_modern_cpp
  ];
}

