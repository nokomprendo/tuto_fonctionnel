#include "Animal.hpp"
#include "View.hpp"

#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>

using namespace Pistache;
using namespace std;

// define server app
class App : public Http::Endpoint {

  private:
    Rest::Router router;

  public:
    App(Address addr) : Http::Endpoint(addr) {

      auto opts = Http::Endpoint::options()
        .flags(Tcp::Options::InstallSignalHandler)
        .flags(Tcp::Options::ReuseAddr);
      init(opts);

      // create a route for the about page
      Rest::Routes::Get(router, "/about", 
        [=](const Rest::Request &, Http::ResponseWriter response) {
            response.send(Http::Code::Ok, renderAbout());
            return Rest::Route::Result::Ok;
        });

      // create a route for the home page
      Rest::Routes::Get(router, "/", 
        [=](const Rest::Request & request, Http::ResponseWriter response) {
            auto myquery = request.query().get("myquery").getOrElse("");
            const vector<Animal> animals = getAnimals(myquery);
            response.send(Http::Code::Ok, renderHome(move(myquery), move(animals)));
            return Rest::Route::Result::Ok;
        });

      // create a route for serving static files
      Rest::Routes::Get(router, "/static/:filename", 
        [=](const Rest::Request & request, Http::ResponseWriter response) {
            auto filename = request.param(":filename").as<string>();
            // the Pistache API is non-blocking; for example, serveFile returns 
            // a Promise, for attaching a callback function
            Http::serveFile(response, "static/" + filename)
              .then(
                  [=](ssize_t s){ cout << filename << " (" << s << " bytes)" << endl; },
                  Async::NoExcept);
            return Rest::Route::Result::Ok;
        });

      setHandler(router.handler());
    }
};

// run server app on port 3000
int main() {
  App app({Ipv4::any(), 3000});
  app.serve();
}

