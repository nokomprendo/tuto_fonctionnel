#include "Animal.hpp"
#include "View.hpp"

#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>

using namespace Pistache;
using namespace std;

class AnimalsApp {

  private:
    Http::Endpoint httpEndpoint;
    Rest::Router router;

  private:
    void aboutR(const Rest::Request &, Http::ResponseWriter response) {
      response.send(Http::Code::Ok, renderAbout());
    }

    void homeR(const Rest::Request & request, Http::ResponseWriter response) {
      auto myquery = request.query().get("myquery").getOrElse("");
      const vector<Animal> animals = getAnimals(myquery);
      response.send(Http::Code::Ok, renderHome(myquery, animals));
    }

    void staticR(const Rest::Request & request, Http::ResponseWriter response) {
      auto filename = request.param(":filename").as<string>();
      Http::serveFile(response, "static/" + filename)
        .then(
            [=](ssize_t s){ cout << filename << " (" << s << " bytes)" << endl; },
            Async::NoExcept);
    }

  public:
    AnimalsApp(Address addr) : httpEndpoint(addr) {

      auto opts = Http::Endpoint::options()
        .flags(Tcp::Options::InstallSignalHandler)
        .flags(Tcp::Options::ReuseAddr);

      httpEndpoint.init(opts);

      /*
      Rest::Routes::Get(router, "/about", 
              [=](const Rest::Request, Http::ResponseWriter response) {
                  response.send(Http::Code::Ok, renderAbout());
                  return Rest::Route::Result::Ok;
              });
      */

      Rest::Routes::Get(router, "/about", 
          Rest::Routes::bind(&AnimalsApp::aboutR, this));

      Rest::Routes::Get(router, "/", 
          Rest::Routes::bind(&AnimalsApp::homeR, this));

      Rest::Routes::Get(router, "/static/:filename", 
          Rest::Routes::bind(&AnimalsApp::staticR, this));

      httpEndpoint.setHandler(router.handler());
    }

    void serve() {
      httpEndpoint.serve();
    }
};

int main() {
  AnimalsApp app({Ipv4::any(), 3000});
  app.serve();
}


