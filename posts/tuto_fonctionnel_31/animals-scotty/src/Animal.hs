{-# LANGUAGE OverloadedStrings #-}

module Animal where

import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as SQL 
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

-- Animal datatype
data Animal = Animal 
    { animalName :: L.Text
    , animalImage :: L.Text
    } deriving Show

-- deserialize an Animal from the database
instance FromRow Animal where
    fromRow = Animal <$> field <*> field 

-- query database (select animals whose name begins with myquery)
getAnimals :: L.Text -> IO [Animal]
getAnimals myquery = do
    let req = "SELECT name,image FROM animals WHERE name LIKE ?||'%'" 
    SQL.withConnection "animals.db" 
        (\conn -> SQL.query conn req (SQL.Only myquery))

