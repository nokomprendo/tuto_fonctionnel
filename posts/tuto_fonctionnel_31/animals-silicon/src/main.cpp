#include <silicon/api.hh>
#include <silicon/backends/mhd.hh>
#include <silicon/middlewares/sqlite_connection.hh>
#include <silicon/middleware_factories.hh>
#include "symbols.hh"

using namespace sl;
using namespace std;

// Model

struct Animal {
  string name;
  string image;
};

using AnimalSql = 
    decltype(D(
        _id = int(),
        _name = string(),
        _image = string()));

vector<Animal> getAnimals(const string & myquery, sqlite_connection & c) {
  vector<Animal> animals;

  c("SELECT name,image from animals where name LIKE ?||'%'")(myquery) | 
      [&animals] (AnimalSql a) { animals.push_back({a.name, a.image}); };

  return animals;
}

// View

string renderAbout() {
  return R"(
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="mystatic/style.css">
      </head> 
      <body>
        <h1>About (Silicon)</h1>
        <p>Generated by <a href="http://siliconframework.org/">Silicon</a></p>
        <p><a href="/animals">Home</a></p>
      </body>
    </html>
  )";
}

string renderHome(const string & myquery, const vector<Animal> & animals) {

  // create a string stream
  ostringstream oss;

  // generate some HTML code, in the stream
  oss << R"(
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="mystatic/style.css">
      </head>
      <body>
        <h1>Animals (Silicon)</h1>
        <form>
          <p> <input type="text" name="myquery" value=")" << myquery << R"("> </p>
        </form>
    )";

  for (const Animal & a : animals) {
    oss << R"(
        <a href="mystatic/)" << a.image << R"(">
            <div class="divCss">
              <p>)" << a.name << R"(</p>
              <img class="imgCss" src="mystatic/)" << a.image << R"(" />
            </div>
          </a>)";
  }

  oss << R"(
        <p style="clear: both"><a href="/about">About</a></p>
      </body>
    </html>
  )";

  // return the resulting string
  return oss.str();
}

// Application

int main() {

  // create app
  auto api = http_api(

    // serve the about page
    GET / _about = [] () { return renderAbout(); },

    // serve the home page (and filter the animals using the "myquery" parameter)
    GET / _animals * get_parameters(_myquery = optional(string())) =
      [] (const auto & p, sqlite_connection & c) {
        vector<Animal> animals = getAnimals(p.myquery, c);
        return renderHome(p.myquery, animals);
    },

    // serve static files (located in the "mystatic" directory)
    GET / _mystatic = file_server("./mystatic")

  );

  // create a connection factory to the database 
  auto factory = middleware_factories( sqlite_connection_factory("animals.db") );

  // run a server on port 3000
  mhd_json_serve(api, factory, 3000);
}

