{ stdenv, fetchurl, openssl }:

stdenv.mkDerivation rec {
  version = "3.0rc2";
  name = "cxxtools-${version}";

  src = fetchurl {
    url = "http://www.tntnet.org/download/${name}.tar.gz";
    sha256 = "0rxni0vx3qw8rk5f0pn0lfmkayndkfbbs9m1i6gnldph1rj799g9";
  };

  buildInputs = [ openssl ];

  enableParallelBuilding = true;
}
