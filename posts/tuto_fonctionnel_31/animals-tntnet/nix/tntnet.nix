{ stdenv, fetchurl, cxxtools, zlib, openssl, zip }:

stdenv.mkDerivation rec {
  name = "tntnet-${version}";
  version = "3.0rc2";

  src = fetchurl {
    url = "http://www.tntnet.org/download/${name}.tar.gz";
    sha256 = "0k81ah8qhkj25gr2wjy0bpdphcm1bi22dkyffxqw0qfhlibfkvbh";
  };

  buildInputs = [ cxxtools zlib openssl zip ];

  enableParallelBuilding = true;
}

