with import <nixpkgs> {};

let
  _cxxtools = callPackage ./nix/cxxtools.nix {};
  _tntdb = callPackage ./nix/tntdb.nix { cxxtools = _cxxtools; };
  _tntnet = callPackage ./nix/tntnet.nix { cxxtools = _cxxtools; };
in

mkShell {
  buildInputs = [
    _cxxtools
    gnumake
    sqlite
    _tntdb
    _tntnet
  ];
}

