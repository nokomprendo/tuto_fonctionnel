#include "Animal.hpp"

#include <cxxtools/serializationinfo.h>
#include <iostream>
#include <tntdb.h>
#include <tntdb/serialization.h>

using namespace std;

// deserialize an Animal from the database (require tntnet >= 3.0)
void operator>>= (const cxxtools::SerializationInfo & si, Animal & a) {
  si.getMember("name") >>= a.name;
  si.getMember("image") >>= a.image;
}

vector<Animal> getAnimals(const string & myquery) {

  vector<Animal> animals;

  try {
    // query database
    tntdb::Connection conn = tntdb::connect("sqlite:animals.db");
    tntdb::Statement sel = conn.prepare(R"(
      SELECT name, image
      FROM animals
      WHERE name LIKE :myquery||'%')"
    );
    sel.set("myquery", myquery);

    // fetch results
    for (tntdb::Statement::const_iterator it = sel.begin(); it != sel.end(); ++it) {
      Animal animal;
      *it >>= animal;
      animals.push_back(animal);
    }
  }
  catch (const exception & e) {
    cerr << e.what() << endl;
  }

  return animals;
};

