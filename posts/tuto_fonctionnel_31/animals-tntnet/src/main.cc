#include <tnt/tntnet.h>

// run server on port 3000
int main() {
  try {
    tnt::Tntnet app;
    app.listen(3000);
    app.mapUrl("^/$", "home");
    app.mapUrl("^/about$", "about");
    app.mapUrl("^/myimg$", "myimg");
    app.mapUrl("^/(static/.*)", "$1", "static@tntnet");
    app.run();
    return 0;
  }
  catch (const std::exception & e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }
}

