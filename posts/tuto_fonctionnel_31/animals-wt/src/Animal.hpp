#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <Wt/Dbo/Dbo.h>

// Animal datatype
struct Animal {
  std::string name;
  std::string image;
};

// ORM
struct AnimalDb : Animal {
    template<class Action> void persist(Action & a) {
        Wt::Dbo::field(a, name, "name");
        Wt::Dbo::field(a, image, "image");
    }
};

#endif

