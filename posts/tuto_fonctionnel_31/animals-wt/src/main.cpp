#include "AboutApp.hpp"
#include "HomeApp.hpp"

#include <Wt/WServer.h>

using namespace std;
using namespace Wt;

int main(int argc, char ** argv) {
  try {
    WServer server(argc, argv, WTHTTP_CONFIGURATION);

    // route the url "/about" to an application "AboutApp"
    server.addEntryPoint(EntryPointType::Application, 
        [](const WEnvironment & env)
        { return make_unique<AboutApp>(env); },
        "/about");

    // route the url "/" to an application "HomeApp"
    server.addEntryPoint(EntryPointType::Application, 
        [=](const WEnvironment & env)
        { return make_unique<HomeApp>(env); },
        "/");

    server.run();
  }
  catch (Dbo::Exception & e) {
    cerr << "Dbo::Exception: " << e.what() << endl;
  }
  return 0;
}

