---
title: Quelques frameworks web C++ (2/2)
---


# Quelques frameworks web C++ (2/2)


Sommaire :

- [Exemple d’application](article-fr-1.html#exemple-dapplication)
    - [Application finale](article-fr-1.html#application-finale)
    - [Architecture MVC](article-fr-1.html#architecture-mvc)
    - [Exemple en Javascript](article-fr-1.html#exemple-en-javascript)
    - [Exemple en Haskell](article-fr-1.html#exemple-en-haskell)
- [Génération de HTML](article-fr-1.html#génération-de-html)
    - [Les générateurs de documents HTML](article-fr-1.html#les-générateurs-de-documents-html)
    - [Les systèmes de templating](article-fr-1.html#les-systèmes-de-templating)
    - [Génération « à la main »](article-fr-1.html#génération-à-la-main)
- [Accès à une base de données SQL](article-fr-1.html#accès-à-une-base-de-données-sql)
    - [Les connecteurs SQL](article-fr-1.html#les-connecteurs-sql)
    - [Les ORM](article-fr-1.html#les-orm)
- [Les frameworks web](article-fr-2.html#les-frameworks-web)
    - [Les micro-frameworks, à la Sinatra/Flask](article-fr-2.html#les-micro-frameworks-à-la-sinatraflask)
    - [Les frameworks asynchrones, à la Node.js](article-fr-2.html#les-frameworks-asynchrones-à-la-node.js)
    - [Les frameworks MVC, à la RoR/Django](article-fr-2.html#les-frameworks-mvc-à-la-rordjango)
    - [Les frameworks basés templates, à la PHP](article-fr-2.html#les-frameworks-basés-templates-à-la-php)
    - [Les frameworks basés widgets](article-fr-2.html#les-frameworks-basés-widgets)
- [Intégration avec Nix](article-fr-2.html#intégration-avec-nix)
- [Conclusion](article-fr-2.html#conclusion)
- [Annexe : résumé des projets et des frameworks présentés](article-fr-2.html#annexe-résumé-des-projets-et-des-frameworks-présentés)



## Les frameworks web


### Les micro-frameworks, à la Sinatra/Flask

Les micro-frameworks web, comme Sinatra en Ruby ou Flask en Python, ont pour
objectif d'être simples et légers. Ils proposent principalement des
fonctionnalités pour traiter des requêtes HTTP ainsi qu'un mécanisme de routage
d'URL.  Si nécessaire, ils peuvent être complétés par d'autres bibliothèques
(génération de HTML, accès à une base SQL…).

Il existe plusieurs micro-frameworks C++, par exemple 
[crow](https://github.com/ipkn/crow) (voir 
[animals-crow](https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_31/animals-crow/src/main.cpp)) ou 
[silicon](http://siliconframework.org/) (voir
[animals-silicon](https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_31/animals-silicon/src/main.cpp)) :

```cpp
#include <silicon/api.hh>
#include <silicon/backends/mhd.hh>
#include <silicon/middlewares/sqlite_connection.hh>
#include <silicon/middleware_factories.hh>
#include "symbols.hh"

using namespace sl;
using namespace std;

...

int main() {

  // create app
  auto api = http_api(

    // serve the about page
    GET / _about = [] () { return renderAbout(); },

    // serve the home page (and filter the animals using the "myquery" parameter)
    GET / _animals * get_parameters(_myquery = optional(string())) =
      [] (const auto & p, sqlite_connection & c) {
        vector<Animal> animals = getAnimals(p.myquery, c);
        return renderHome(p.myquery, animals);
      },

    // serve static files (located in the "mystatic" directory)
    GET / _mystatic = file_server("./mystatic")

  );

  // create a connection factory to the database 
  auto factory = middleware_factories( sqlite_connection_factory("animals.db") );

  // run a server on port 3000
  mhd_json_serve(api, factory, 3000);
}
```

Ici, les fonctionnalités du C++ moderne rende le code concis et plutôt agréable
à lire (par exemple la lambda pour la route `_animals`).

Dans une phase de prétraitement, Silicon génère le fichier `symbols.hh`, qui
déclare les symboles définis par le programmeur, notamment les routes
(`_about`, `_home`, `_mystatic`…). Ceci permet de vérifier statiquement que les
routes sont utilisées correctement dans le code.  D'autres langages utilisent
l'introspection pour effectuer ce genre de vérification mais C++ ne possède
pas cette fonctionnalité.



### Les frameworks asynchrones, à la Node.js

Les frameworks asynchrones, comme Node.js/Express en Javascript, proposent les
mêmes fonctionnalités que les micro-frameworks classiques mais via des
fonctions non-bloquantes.  Ainsi, si une requête a besoin d'une ressource,
l'application peut passer à une autre requête en attendant que la ressource
soit disponible.  Ceci permet d'améliorer les performances générales de
l'application mais nécessite un style de programmation particulier, à base de
*promesses* connectées à des *fonctions callbacks* par des *then* pour former
une chaine de traitements asynchrones.

Il existe différents frameworks asynchrones en C++, par exemple
[cpprestsdk](https://github.com/Microsoft/cpprestsdk) 
(voir 
[animals-cpprestsdk](https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_31/animals-cpprestsdk/src/main.cpp))
et
[pistache](http://pistache.io/) (voir 
[animals-pistache](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-pistache/src/main.cpp)) :


```cpp
#include "Animal.hpp"
#include "View.hpp"

#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>

using namespace Pistache;
using namespace std;

// define server app
class App : public Http::Endpoint {

  private:
    Rest::Router router;

  public:
    App(Address addr) : Http::Endpoint(addr) {

      auto opts = Http::Endpoint::options()
        .flags(Tcp::Options::InstallSignalHandler)
        .flags(Tcp::Options::ReuseAddr);
      init(opts);

      // create a route for the about page
      Rest::Routes::Get(router, "/about", 
        [=](const Rest::Request &, Http::ResponseWriter response) {
            response.send(Http::Code::Ok, renderAbout());
            return Rest::Route::Result::Ok;
        });

      // create a route for the home page
      Rest::Routes::Get(router, "/", 
        [=](const Rest::Request & request, Http::ResponseWriter response) {
            auto myquery = request.query().get("myquery").getOrElse("");
            const vector<Animal> animals = getAnimals(myquery);
            response.send(Http::Code::Ok, renderHome(move(myquery), move(animals)));
            return Rest::Route::Result::Ok;
        });

      // create a route for serving static files
      Rest::Routes::Get(router, "/static/:filename", 
        [=](const Rest::Request & request, Http::ResponseWriter response) {
            auto filename = request.param(":filename").as<string>();
            // the Pistache API is non-blocking; for example, serveFile returns 
            // a Promise, for attaching a callback function
            Http::serveFile(response, "static/" + filename)
              .then(
                  [=](ssize_t s){ cout << filename << " (" << s << " bytes)" << endl; },
                  Async::NoExcept);
            return Rest::Route::Result::Ok;
        });

      setHandler(router.handler());
    }
};

// run server app on port 3000
int main() {
  App app({Ipv4::any(), 3000});
  app.serve();
}
```

On retrouve ici une gestion classique des routes (avec le nom de la route et sa
fonction de traitement). Cependant, on a désormais un fonctionnement asynchrone,
via des fonctions non bloquantes. Par exemple pour la route "static", la
fonction `serveFile` retourne une promesse que l'on connecte à une fonction
callback, qui affiche un message de log une fois la promesse résolue.





### Les frameworks MVC, à la RoR/Django

Les frameworks web MVC, comme Ruby on Rails ou Python Django, sont des outils
classiques dont l'objectif est d'implémenter tout type d'application
web. Ils fournissent généralement toutes les fonctionnalités nécessaires :
routage d'URL, système de templating, accès à des bases de données, système
d'authentification…
Les frameworks MVC ne semblent pas être le domaine de prédilection du C++ mais
on trouve tout de même quelques outils intéressants, notamment
[cppcms](http://cppcms.com/wikipp/en/page/main/). 

En plus des fonctionnalités classiques d'un framework MVC, cppcms propose un
système de templating assez évolué, avec héritage de vues et gestion de
contenu.  Par exemple, on peut définir une vue principale `MasterView` et en
dériver des vues `AboutView` et `HomeView` qui héritent des caractéristiques de
`MasterView` et les complètent. Enfin, on peut associer un contenu à
ces vues (paramètres des templates), également avec un système d'héritage.  En
reprenant l'exemple précédent, on peut définir un contenu `MasterContent` pour
la vue `MasterView`, la dériver en `HomeContent` pour la vue `HomeView` et
utiliser directement `MasterContent` pour la vue `AboutView` (pas de nouveau
paramètre dans le template).

Au niveau du code, le fichier
[animals-cppcms/src/content.h](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms/src/content.h)
définit les contenus :

```cpp
// define how to exchange data between the C++ code and the templates
namespace content  {

  // content for MasterView and AboutView
  struct MasterContent : cppcms::base_content {
    std::string title;  // the "title" parameter in master.tmpl
  };

  // datatype for the form in HomeView
  struct InfoForm : cppcms::form {
    cppcms::widgets::text myquery; 
    InfoForm() {
      add(myquery);
    }
  };

  // content for HomeView 
  // inherits from MasterContent, because HomeView inherits from MasterView
  struct HomeContent : MasterContent {
    std::vector<Animal> animals;  // the "animals" parameter in home.tmpl
    InfoForm info;  // the "info" parameter in home.tmpl
  };

}
```

Le fichier 
[animals-cppcms/src/master.tmpl](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms/src/master.tmpl) 
définit la vue `MasterView` :

```html
<% c++ #include "content.h" %>

<% skin myskin %>
  <% view MasterView uses content::MasterContent %>

    <!-- "title" is set in the C++ code and used in the "render" template below -->
    <% template title() %> <%= title %> <% end %>

    <!-- "page_content" is set in the sub-template and used in the "render" template below -->
    <% template page_content() %> to be overriden in sub-templates <% end %>

    <!-- main template that uses the two previous ones and that is called from the C++ code -->
    <% template render() %>
      <html>

        <head>
          <style>
            body {
              background-color: azure;
            }
            ...
          </style>
        </head>

        <body>
          <!-- adds the "title" template defined above  -->
          <h1><% include title() %></h1>
          <!-- adds the "page_content" template defined above  -->
          <div> <% include page_content() %> </div>
        </body>

      </html>
    <% end template %> 

  <% end view %>
<% end skin %>

```

Le fichier 
[animals-cppcms/src/about.tmpl](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms/src/about.tmpl) 
définit la vue `AboutView` :

```html
<% skin myskin %>

  <!-- inherits from MasterView -->
  <% view AboutView uses content::MasterContent extends MasterView %>

    <!-- defines "page_content", that is used in MasterView -->
    <% template page_content() %>
      <p>Generated by <a href='http://cppcms.com/wikipp/en/page/main'>Cppcms</a></p>
      <p><a href='<% url "/" %>'>Home</a></p>
    <% end template %> 

  <% end view %>

<% end skin %>
```

Le fichier 
[animals-cppcms/src/about.tmpl](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms/src/about.tmpl) 
définit la vue `HomeView` :

```html
<% skin myskin %>

  <!-- inherits from MasterView -->
  <% view HomeView uses content::HomeContent extends MasterView %>

    <!-- defines "page_content", that is used in MasterView -->
    <% template page_content() %>

      <!-- creates a form using the "info" attribute defined in HomeContent -->
      <form method="get" action="" >
        <% form as_p info %>
      </form>

      <!-- add the HTML elements corresponding to the "animals" attribute defined in HomeContent -->
      <% foreach animal in animals %>
        <% item %>
          <a class="aCss" href="img/<%= animal.image %>" >
            <div class="divCss">
            <p><%= animal.name %></p>
              <img class="imgCss" src="img/<%= animal.image %>" />
            </div>
          </a>
        <% end %>
      <% end foreach %>

      <p style="clear:both"><a href='<% url "/about" %>'>About</a></p>

    <% end template %> 

  <% end view %>
<% end skin %>
```

Enfin le programme principal définit le routage d'URL et 
initialise les contenus avant de lancer le rendu des vues. Fichier
[animals-cppcms/src/main.cpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms/src/main.cpp) :

```cpp
// main application
class App : public cppcms::application {

  public:
    App(cppcms::service &srv) : cppcms::application(srv) {
      // about page
      dispatcher().assign("/about", &App::about, this);
      mapper().assign("about","/about");
      // home page
      dispatcher().assign("", &App::home, this);
      mapper().assign("");
      // images
      dispatcher().assign("/img/([a-z_0-9_\\-]+\\.jpg)", &App::serveJpg, this, 1);
      // root url
      mapper().root("/animals");
    }

  private:
    void about() {
      // AboutView inherits from MasterView and uses the same content type (MasterContent)
      content::MasterContent c;
      c.title = "About (Cppcms)";
      // render the AboutView template
      render("AboutView", c);
    }

    void home()  {
      // HomeView inherits from MasterView and uses its own content type 
      // (HomeContent, that inherits from MasterContent)
      content::HomeContent c;
      // data defined in MasterContent 
      c.title = "Animals (Cppcms)";
      // data defined in HomeContent
      c.info.load(context());
      c.animals = getAnimals(c.info.myquery.value());
      // render the HomeView template
      render("HomeView", c);
    }

    void serveJpg(string filename)  {
      // open and send the image file
      ifstream ifs("img/" + filename);
      if (ifs) {
        response().content_type("image/jpeg");
        response().out() << ifs.rdbuf();
      }
      else {
        response().status(404);
      }
    }
};

// create and run the application
int main(int argc, char ** argv) {
  try {
    cppcms::service srv(argc, argv);
    srv.applications_pool().mount(cppcms::applications_factory<App>());
    srv.run();
  }
  catch(exception const & e) {
    cerr << e.what() << endl;
  }
  return 0;
}
```

Les frameworks MVC sont des outils efficaces pour implémenter des applications
complexes. Cependant, ils nécessitent un apprentissage assez conséquent et 
peuvent être surdimensionnés pour des petites applications simples.





### Les frameworks basés templates, à la PHP

Le framework 
[tntnet](http://www.tntnet.org/tntnet.html)
propose un système basé templates, à la manière de PHP.
Si ce framework est assez anecdotique dans l'ecosystème C++, il semble
cependant plutôt efficace dans son approche : écrire du code HTML classique
et y ajouter des sections de code C++ là où c'est nécessaire.

Par exemple, le fichier 
[animals-tntent/src/myimg.ecpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-tntnet/src/myimg.ecpp)
définit une application qui affiche une image dont le nom est passé en paramètre :

```html
<%args>
  filename;
</%args>

<html>
  <body>
    <img src="static/img/<$filename$>" />
  </body>
</html>
```

De même, le fichier 
[animals-tntent/src/home.ecpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-tntnet/src/home.ecpp)
définit une application plus complexe (appel de fonction C++, génération de
code HTML via une boucle en C++…) :

```html
<%args>
  myquery;
</%args>

<%pre>
  #include "Animal.hpp"
</%pre>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="static/style.css">
  </head>

  <body>

    <h1>Animals (Tntnet)</h1>

    <form>
      <p> <input type="text" name="myquery" value="<$myquery$>"> </p>
    </form>

    <%cpp> for (const Animal & animal : getAnimals(myquery)) { </%cpp>

      <a href="myimg?filename=<$animal.image$>">
        <div class="divCss">
          <p> <$animal.name$> </p>
          <img class="imgCss" src="static/img/<$animal.image$>" />
        </div>
      </a>

    <%cpp> } </%cpp>

    <p style="clear: both"><a href="/about">About</a></p>

  </body>
</html>
```

Enfin, tntnet propose différents types de déploiement : programme CGI, 
serveur autonome, serveur d'applications tntnet compilées dans des bibliothèques
dynamiques. Par exemple, pour implémenter un serveur autonome 
([animals-tntent/src/main.cpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-tntnet/src/main.cpp)) :

```cpp
#include <tnt/tntnet.h>

// run server on port 3000
int main() {
  try {
    tnt::Tntnet app;
    app.listen(3000);
    app.mapUrl("^/$", "home");        // route the "/" url to the "home" application 
    app.mapUrl("^/about$", "about");  // route the "/about" url to the "about" application 
    app.mapUrl("^/myimg$", "myimg");  // ...
    app.mapUrl("^/(static/.*)", "$1", "static@tntnet");
    app.run();
    return 0;
  }
  catch (const std::exception & e) {
    std::cerr << e.what() << std::endl;
    return -1;
  }
}
```

À noter que ce type de framework est peut-être moins adapté au développement
d'applications complexes (lisibilité des templates, réutilisation…).



### Les frameworks basés widgets

Ces outils s'inspirent des frameworks d'interfaces graphiques de bureau, comme
[Qt](https://www.qt.io/) ou [gtkmm](https://www.gtkmm.org/), c'est-à-dire basés
sur une hiérarchie de widgets composant l'interface et intéragissant via un
mécanisme de signal-slot.

Les frameworks web basés widgets sont étonnament peu répandus, même tous
langages confondus, alors que leur potentiel semble important. En effet, ils
permettent de développer une application fullstack client-serveur en utilisant
une bibliothèque d'interface graphique classique et sans avoir à trop se
préoccuper de l'architecture réseau de l'application.

En C++, le framework le plus abouti dans cette catégorie est certainement
[Wt](https://www.webtoolkit.eu/wt/). Wt possède de nombreux widgets classiques
ou évolué, un ORM SQL, un système d'authentification, la possibilité de
manipuler du HTML/CSS… En Wt, le programme principale se résume à router des
URL vers les applications correspondantes (animals-wt/src/main.cpp) : 

```cpp
...

int main(int argc, char ** argv) {
  try {
    WServer server(argc, argv, WTHTTP_CONFIGURATION);

    // route the url "/about" to an application "AboutApp"
    server.addEntryPoint(EntryPointType::Application, 
        [](const WEnvironment & env)
        { return make_unique<AboutApp>(env); },
        "/about");

    // route the url "/" to an application "HomeApp"
    server.addEntryPoint(EntryPointType::Application, 
        [=](const WEnvironment & env)
        { return make_unique<HomeApp>(env); },
        "/");

    server.run();
  }
  catch (Dbo::Exception & e) {
    cerr << "Dbo::Exception: " << e.what() << endl;
  }
  return 0;
}

```

Ces applications Wt correspondent à des interfaces graphiques classiques mais 
avec une architecture client-serveur. Par exemple pour définir l'application "about" (page statique) via le système de template HTML/CSS, il
suffit de définir la classe suivante ([animals-wt/src/AboutApp.hpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-wt/src/AboutApp.hpp)) :

```cpp
...

// Application class implementing the about page
class AboutApp : public Wt::WApplication {
  private:
    // main HTML template of the application
    const std::string _app_template = R"(
        <h1>About (Wt)</h1>
        <p>Generated by <a href="https://www.webtoolkit.eu/wt">Wt</a></p>
        <p><a href="/">Home</a></p>
    )";

  public:
    // create the application
    AboutApp(const Wt::WEnvironment & env) : Wt::WApplication(env) {
      // load css
      useStyleSheet({"style.css"});
      // create the main widget using the HTML template
      root()->addWidget(std::make_unique<Wt::WTemplate>(_app_template));
    }
};
```

Pour une application plus complexe, par exemple la page affichant les animaux,
on peut définir un nouveau widget `AnimalWidget` qui implémente une vignette,
puis utiliser cette classe pour afficher tous les animaux lus dans la base de
données (voir [animals-wt/src/HomeApp.hpp](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-wt/src/HomeApp.hpp)) :

```cpp
...

bool isPrefixOf(const std::string & txt, const std::string & fullTxt) {
  return std::inner_product(std::begin(txt), std::end(txt), std::begin(fullTxt), 
      true, std::logical_and<char>(), std::equal_to<char>());
} 

// widget showing an animal (name + image + anchor) 
class AnimalWidget : public Wt::WAnchor {
  private:
    // pointer to the WText that contains the animal name
      Wt::WText * _animalName;

  public:
    AnimalWidget(const Animal & animal) {
      // set anchor href
      const std::string imagePath = "img/" + animal.image;
      setLink(Wt::WLink(imagePath));
      // create a container widget, inside the anchor widget
      auto cAnimal = addWidget(std::make_unique<Wt::WContainerWidget>());
      cAnimal->setStyleClass("divCss");
      // create a text widget, inside the container
      auto cText = cAnimal->addWidget(std::make_unique<Wt::WContainerWidget>());
      cText->setPadding(Wt::WLength("1em"));
      _animalName = cText->addWidget(std::make_unique<Wt::WText>(animal.name));
      // create an image widget, inside the container
      auto img = cAnimal->addWidget(std::make_unique<Wt::WImage>(imagePath));
      img->setStyleClass("imgCss");
    }

    void filter(const std::string & txt) {
      // show the widget if txt is null or if it is a prefix of the animal name
      setHidden(txt != "" and not isPrefixOf(txt, _animalName->text().toUTF8()));
    }
};

// Application class implementing the home page
class HomeApp : public Wt::WApplication {
  private:
    // the line edit widget (for querying animal to show/hide)
      Wt::WLineEdit * _myquery;

    // the animal widgets 
    std::vector<AnimalWidget*> _animalWidgets;

    // main HTML template of the application
    const std::string _app_template = R"(
        <h1>Animals (Wt)</h1>
        <p>${myquery}</p>
        ${animals}
        <p style="clear: both"><a href="/about">About</a></p>
    )";

    // show all animals that match the _myquery prefix
    void filterAnimals() {
      for(auto aw : _animalWidgets)
        aw->filter(_myquery->text().toUTF8());
    }

  public:
    // create the application
    HomeApp(const Wt::WEnvironment & env) : WApplication(env) {
      // load css
      useStyleSheet({"style.css"});
      // create the main widget using the HTML template
      auto r = root()->addWidget(std::make_unique<Wt::WTemplate>(_app_template));
      // create the remaining widgets and bind them to the template placeholders
      _myquery = r->bindWidget("myquery", std::make_unique<Wt::WLineEdit>());
      // connect the widget _myquery to the function filterAnimals 
      _myquery->textInput().connect(this, &HomeApp::filterAnimals);

      // create a container widget for the animals
      auto w = r->bindWidget("animals", std::make_unique<Wt::WContainerWidget>());
      // open the database
      Wt::Dbo::Session session;
      session.setConnection(std::make_unique<Wt::Dbo::backend::Sqlite3>("animals.db"));
      session.mapClass<AnimalDb>("animals");
      // query the database
      Wt::Dbo::Transaction transaction(session);
      Wt::Dbo::collection<Wt::Dbo::ptr<AnimalDb>> dboAnimals = session.find<AnimalDb>();
      for (const Wt::Dbo::ptr<AnimalDb> & dboAnimal : dboAnimals) {
        // add a widget
        auto aw = w->addWidget(std::make_unique<AnimalWidget>(*dboAnimal));
        // store a pointer, for future updates
        _animalWidgets.push_back(aw);
      }
    }
};
```

À première vue, cette implémentation peut sembler plus longue et plus
compliquée que les implémentations précédentes. Cependant, son code devrait
sembler familier à n'importe quel développeur d'interface graphique de bureau.
De plus, cette implémentation gère l'ensemble de l'application (fullstack), et
non la partie serveur uniquement.  Par exemple, la connexion du signal
`_myquery->textInput()` à la fonction `HomeApp::filterAnimals` implique des
mises-à-jour en temps-réel côté client, ce qui serait nettement plus difficile
à implémenter avec les frameworks précédents.



## Intégration avec Nix

Avant de conclure, voici une petite remarque concernant l'intégration, dans un
projet de code, des frameworks présentés. En effet, ces frameworks sont
rarement présents dans les logithèques des OS et doivent donc généralement être
installées manuellement. Pour cela, on choisit classiquement l'une des deux
solutions suivantes :

- télécharger le code source de la bibliothèque, l'installer dans un dossier
  local ou système et régler les variables d'environnement de façon à trouver
la bibliothèque;

- intégrer la bibliothèque directement au projet, par exemple via les
  sous-modules git, et configurer le projet de façon à trouver la bibliothèque.

Aucune de ces solutions n'est vraiment satisfaisante : la première solution
introduit une dépendance externe peu portable qu'il faut mettre à jour
manuellement; la seconde solution introduit des duplications de code
potentielles et nécessite une configuration de projet particulière.

Une troisième solution, particulièrement avantageuse, consiste à utiliser le 
système de paquets [Nix](https://nixos.org/nix/). Par exemple, pour récupérer et empaquetter cpprestsdk depuis son projet github, il suffit d'écrire le fichier Nix suivant (voir
[animals-cpprestsdk/nix/cpprestsdk.nix](https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_31/animals-cpprestsdk/nix/cpprestsdk.nix)) :

```nix
{ stdenv, fetchFromGitHub, cmake, boost, openssl, websocketpp, zlib }:

stdenv.mkDerivation {

  name = "cpprestsdk";

  src = fetchFromGitHub {
    owner = "Microsoft";
    repo = "cpprestsdk";
    rev = "204a52610234ac5180e80a6883b62c0ad085f51e";
    sha256 = "0mj2m6n889zdhwxdx24ljxfqryivvn3w9vzs94ppzcx5apa5jb3w";
  };

  enableParallelBuilding = true;

  buildInputs = [ boost cmake openssl websocketpp zlib ];
}
```

Pour inclure, dans un projet, des frameworks ainsi empaquetés, il suffit alors de les
appeler dans le fichier de configuration `default.nix` du projet 
(voir [animals-cpprestsdk/default.nix](https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_31/animals-cpprestsdk/default.nix)) :

```nix
with import <nixpkgs> {};

let
  _cpprestsdk = callPackage ./nix/cpprestsdk.nix {};
  _ctml = callPackage ./nix/ctml.nix {};
  _sqlite_orm = callPackage ./nix/sqlite_orm.nix {};
in

stdenv.mkDerivation {
  name = "animals-cpprestsdk";
  src = ./.;

  buildInputs = [
    boost
    _cpprestsdk
    _ctml
    openssl
    sqlite
    _sqlite_orm
  ];

  buildPhase = '' 
    g++ -O2 -o animals-cpprestsdk src/*.cpp -lcpprest -lboost_system -lssl -lsqlite3
    sqlite3 animals.db < animals.sql
  '';

  installPhase = ''
    mkdir -p $out/static
    cp animals-cpprestsdk $out/
    cp animals.db $out/
    cp $src/static/* $out/static/
  '';
}
```

On peut alors facilement compiler et lancer le programme, avec les commandes
suivantes :

```sh
nix-build
cd result
./animals-cpprestsdk
```

Cette méthode à l'avantage de rendre les dépendances explicites et isolées mais
sans duplication.  Elle facilite également la réutilisation et les
mises-à-jour.





## Conclusion

Pour développer des applications web backend, le C++ est une option tout à fait
envisageable. Avec ses dernières évolutions, le langage est généralement plus
simple et plus sûr à utiliser, sans compromis sur les performances.  De
nombreuses bibliothèques C++ sont disponibles pour le développement web :
templating, génération de HTML, connexion SQL, ORM… Les frameworks web sont
également nombreux et variés : framework MVC à la RoR/Django, micro-framework à
la Sinatra/Flask, framework asynchrone à la Node.js, framework basé template à
la PHP, et même framework fullstack basé widget… Enfin, on notera qu'avec Nix,
il est très facile de configurer un projet intégrant ce genre de bibliothèques.
Bien évidemment, tout ceci interessera essentiellement des développeurs qui
connaissent déjà C++ car beaucoup d'autres langages ont également des outils
très intéressant pour le développement web.





## Annexe : résumé des projets et des frameworks présentés

À noter qu'il s'agit uniquement d'exemples d'associations. Il est généralement
possible d'utiliser n'importe quel framework web avec n'importe quel générateur
HTML et n'importe quelle interface SQL.

| projet | framework web | générateur HTML | interface SQL |
|---|---|---|---|
| [animals-cppcms](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cppcms) | [cppcms](http://cppcms.com/wikipp/en/page/main/) (framework MVC) | cppcms (système de templates) | cppcms (connecteur SQL) |
| [animals-cpprestsdk](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-cpprestsdk) | [cpprestsdk](https://github.com/Microsoft/cpprestsdk) (framework réseau asynchrone) | [ctml](https://github.com/tinfoilboy/CTML) (générateur de documents) | [sqlite_orm](https://github.com/fnc12/sqlite_orm) (ORM) |
| [animals-crow](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-crow) | http: [crow](https://github.com/ipkn/crow) (micro-framework) | crow (système de templates) | [sqlpp11](https://github.com/rbock/sqlpp11) (ORM) |
| [animals-nodejs](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-nodejs) (Javascript/Node.js) | [express](https://expressjs.com/) (micro-framework asynchrone) | [pug](https://pugjs.org) (générateur de documents) | [better-sqlite3](https://github.com/JoshuaWise/better-sqlite3) (connecteur SQL) |
| [animals-pistache](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-pistache) | [pistache](http://pistache.io/) (micro-framework asynchrone) | [kainjow mustache](https://github.com/kainjow/Mustache) (système de templates) | [sqlite_modern_cpp](https://github.com/SqliteModernCpp/sqlite_modern_cpp) (connecteur SQL) |
| [animals-scotty](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-scotty) (Haskell) | [scotty](http://hackage.haskell.org/package/scotty) (micro-framework) | [lucid](https://hackage.haskell.org/package/lucid) and [clay](https://hackage.haskell.org/package/clay) (générateurs de documents) | [sqlite-simple](https://hackage.haskell.org/package/sqlite-simple) (connecteur SQL) |
| [animals-silicon](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-silicon) | [silicon](http://siliconframework.org/) (micro-framework) | aucun | silicon (connecteur SQL) |
| [animals-tntnet](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-tntnet) | [tntnet](http://www.tntnet.org/tntnet.html) (framework basé templates) | tntnet (système de templates) | tntnet (connecteur SQL) |
| [animals-wt](https://framagit.org/nokomprendo/tuto_fonctionnel/tree/master/posts/tuto_fonctionnel_31/animals-wt) | [wt](https://www.webtoolkit.eu/wt) (framework basé widgets) | wt (système de widgets + templates) | wt (ORM) |

[partie précédente](article-fr-1.html)

