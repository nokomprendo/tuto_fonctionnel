# see https://github.com/FPtje/miso-isomorphic-example

let
  nixpkgs-src = fetchTarball {
    # 2018-11-12
    url = "https://github.com/NixOS/nixpkgs/archive/3fd87ad0073fd1ef71a8fcd1a1d1a89392c33d0a.tar.gz";
    sha256 = "0n4ffwwfdybphx1iyqz1p7npk8w4n78f8jr5nq8ldnx2amrkfwhl";
  };

  pkgs = import (nixpkgs-src) {};

  servant-src = pkgs.fetchFromGitHub {
    owner = "haskell-servant";
    repo = "servant";
    rev = "v0.15";
    sha256 = "0n9xn2f61mprnvn9838zbl4dv2ynnl0kxxrcpf5c0igdrks8pqws";
  };

  # Many packages don't build on ghcjs because of a dependency on doctest
  # (which doesn't build), or because of a runtime error during the test run.
  # See: https://github.com/ghcjs/ghcjs/issues/711

  #ghcjs = pkgs.haskell.packages.ghcjs710.override {
  #ghcjs = pkgs.haskell.packages.ghcjs80.override {
  #ghcjs = pkgs.haskell.packages.ghcjs82.override {
  ghcjs = pkgs.haskell.packages.ghcjs84.override {
    overrides = final: previous: with pkgs.haskell.lib; {
      tasty-quickcheck = dontCheck previous.tasty-quickcheck;
      http-types       = dontCheck previous.http-types;
      comonad          = dontCheck previous.comonad;
      semigroupoids    = dontCheck previous.semigroupoids;
      lens             = dontCheck previous.lens;
      servant          = dontCheck (doJailbreak (previous.callCabal2nix "servant" (servant-src + "/servant") {}));
    };
  };

  ghc = pkgs.haskell.packages.ghc844;

  miso-src = pkgs.fetchFromGitHub {
    owner = "haskell-miso";
    repo = "miso";
    #rev = "0.21.1.0";
    #sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    rev = "0.21.2.0";
    sha256 = "07k1rlvl9g027fp2khl9kiwla4rcn9sv8v2dzm0rzf149aal93vn";
  };

  miso-ghc   = ghc.callCabal2nix "miso" miso-src {};
  miso-ghcjs = ghcjs.callCabal2nix "miso" miso-src {};

  draw = ghcjs.callCabal2nix "draw" ./. { miso = miso-ghcjs; };

  draw_pkg = pkgs.runCommand "draw_pkg" { inherit draw; } ''
    mkdir -p $out
    cp ${draw.src}/index.html $out/
    ${pkgs.closurecompiler}/bin/closure-compiler ${draw}/bin/draw.jsexe/all.js > $out/all.js
  '';

in if pkgs.lib.inNixShell then draw.env else draw_pkg

