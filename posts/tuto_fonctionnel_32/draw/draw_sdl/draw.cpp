#include <emscripten.h>
#include <emscripten/bind.h>
#include <SDL2/SDL.h>

const int WIDTH = 640;
const int HEIGHT = 480;

SDL_Window * g_window;
SDL_Renderer * g_renderer;
SDL_Texture * g_texture;  // for rendering to texture

int g_x0 = 0;  // current drawing position 
int g_y0 = 0;
bool g_drawing = false;  // drawing state

// copy texture to displayed canvas
void display_texture() {
    SDL_SetRenderTarget(g_renderer, nullptr);
    SDL_RenderClear(g_renderer);
    SDL_RenderCopy(g_renderer, g_texture, nullptr, nullptr);
    SDL_RenderPresent(g_renderer);
}

// clear and display the texture
void clear() {
    SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 0);
    SDL_SetRenderTarget(g_renderer, g_texture);
    SDL_RenderClear(g_renderer);
    SDL_RenderPresent(g_renderer);
    g_texture = SDL_GetRenderTarget(g_renderer);
    display_texture();
}

// do one iteration of the event loop
void iter_one() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.button.button == SDL_BUTTON_LEFT) {
            if (event.type == SDL_MOUSEBUTTONDOWN) {
                // start drawing 
                g_drawing = true;
                g_x0 = event.button.x;
                g_y0 = event.button.y;
            }
            else if (event.type == SDL_MOUSEBUTTONUP) {
                // stop drawing 
                g_drawing = false;
            }
        }
        else if (event.type == SDL_MOUSEMOTION) {
            if (g_drawing) {
                // draw a line in the texture, from previous position to current position
                SDL_SetRenderDrawColor(g_renderer, 255, 255, 255, 255);
                SDL_SetRenderTarget(g_renderer, g_texture);
                SDL_RenderDrawLine(g_renderer, g_x0, g_y0, event.motion.x, event.motion.y);
                SDL_RenderPresent(g_renderer);
                g_texture = SDL_GetRenderTarget(g_renderer);
                // display texture
                display_texture();
                // update current position 
                g_x0 = event.motion.x;
                g_y0 = event.motion.y;
            }
        }
    }
}

// init SDL and launch the event loop
int main() {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &g_window, &g_renderer);
    g_texture = SDL_CreateTexture(g_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WIDTH, HEIGHT);
    emscripten_set_main_loop(iter_one, 0, 1);
    return 0;
}

// export function to JS
EMSCRIPTEN_BINDINGS(draw_sdl) {
    emscripten::function("clear", &clear);
}

