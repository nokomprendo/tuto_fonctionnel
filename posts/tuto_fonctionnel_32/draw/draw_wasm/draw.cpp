#include <emscripten/bind.h>
#include <cmath>
#include <iostream>

struct Color {
    uint8_t _r;
    uint8_t _g;
    uint8_t _b;
    uint8_t _a;
};

class MyCanvas {
    private:
        // canvas properties
        int _width;
        int _height;
        std::vector<Color> _data;

        // pen properties
        Color _color;
        int _squareRadius;

    public:
        MyCanvas(int width, int height) : 
            _width(width),
            _height(height),
            _data(width*height)
        {
            clear();
        }

        void clear() {
            memset(_data.data(), 255, _width*_height*sizeof(Color));
        }

        emscripten::val getData() { 
            // get C++ data for filling a JS array
            size_t s = _data.size()*4;
            uint8_t * d = (uint8_t *) _data.data();
            return emscripten::val(emscripten::typed_memory_view(s, d));
        }

        void initDraw(int r, int g, int b, int radius) {
            // set current pen properties
            _squareRadius = radius*radius;
            _color = {uint8_t(r), uint8_t(g), uint8_t(b), 255};
        }

        // draw a line from (x0, y0) to (x1, y1)
        void draw(int x0, int y0, int x1, int y1) {
            // draw a disc at each point of the line -> VERY UNOPTIMIZED !!!
            int sx0, sx1, sy0, sy1;

            if (x0 < x1) {
                sx0 = x0;
                sx1 = x1;
                sy0 = y0;
                sy1 = y1;
            }
            else {
                sx0 = x1;
                sx1 = x0;
                sy0 = y1;
                sy1 = y0;
            }

            const double dx = sx1 - sx0;
            const double dy = sy1 - sy0;

            if (dx > fabs(dy)) {
                const double m = dy / dx;
                for (int x=sx0; x<=sx1; x++) {
                    int y = sy0 + m*(x - sx0);
                    drawDisc(x, y);
                }
            }
            else {
                int ymin = sy0;
                int ymax = sy1;
                if (ymin > ymax)
                    std::swap(ymin, ymax);
                double m = sy0 == sy1 ? 0.0 : dx / dy;
                for (int y=ymin; y<=ymax; y++) {
                    int x = sx0 + m*(y - sy0);
                    drawDisc(x, y);
                }
            }
        }

    private:
        // draw a disc at (evt_x, evt_y) using current pen (_color + _squareRadius)
        void drawDisc(int evt_x, int evt_y) { 
            const int x0 = std::max(0, evt_x - _squareRadius);
            const int x1 = std::min(_width, evt_x + _squareRadius);
            const int y0 = std::max(0, evt_y - _squareRadius);
            const int y1 = std::min(_height, evt_y + _squareRadius);
            for (int x=x0; x<x1; x++) {
                for (int y=y0; y<y1; y++) {
                    const int dx = evt_x - x;
                    const int dy = evt_y - y;
                    if (dx*dx + dy*dy < _squareRadius) {
                        const int k = y*_width + x;
                        _data[k] = _color;
                    }
                }
            }
        }
};

// export MyCanvas to JS
EMSCRIPTEN_BINDINGS(MyCanvas) {
    emscripten::class_<MyCanvas>("MyCanvas")
        .constructor<int, int>()
        .function("clear", &MyCanvas::clear)
        .function("initDraw", &MyCanvas::initDraw)
        .function("draw", &MyCanvas::draw)
        .function("getData", &MyCanvas::getData);
}

