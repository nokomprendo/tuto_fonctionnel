with import <nixpkgs> {};

let
  _wt = callPackage ./wt.nix {};
in

mkShell {
  buildInputs = [
    gnumake
    _wt
  ];

  shellHooks = ''
    export WT_ROOT=${_wt}
  '';
}

