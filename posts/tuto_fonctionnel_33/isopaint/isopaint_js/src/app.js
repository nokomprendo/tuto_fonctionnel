"use strict";

const port = 3000;

const express = require("express");
const app = express();

const http = require("http").Server(app);
const io = require("socket.io")(http);

let paths = [];

// serve static files
app.use("/", express.static("./static"));

// client connection
io.on("connection", function(socket){

    // when a new connection is accepted, send all paths
    socket.emit("stoh all paths", paths);

    // when a client sends a new path
    socket.on("htos new path", function(path){
        // store the new path
        paths.push(path);
        // send the new path to all clients
        io.emit("stoh new path", path);
    });
});

http.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});

