{-# LANGUAGE OverloadedStrings          #-}

module Common where

import Miso
import Data.Map (singleton)

-- Model

type Path = [(Double, Double)]

data Model = Model 
    { allPaths_ :: [Path]               -- all the paths, sent by the server
    , currentPath_ :: Path              -- current path (when the user is drawing)
    , currentXy_ :: (Double, Double)    -- last position of the mouse (when drawing)
    , drawing_ :: Bool                  -- set whether the user is drawing or not
    } deriving (Eq, Show)

initialModel :: Model
initialModel = Model [] [] (0,0) False

-- View

homeView :: Model -> View Action
homeView _ = div_ 
    []
    [ p_ [] [ text "isopaint_miso" ]
    , canvas_ 
        [ id_ "canvas_draw" , width_ "400" , height_ "300"
        , style_  (singleton "border" "1px solid black")
        , onMouseDown MouseDown     -- when mouse down, generate a MouseDown action
        , onMouseUp MouseUp         -- when mouse up, generate a MouseUp action
        ]
        []
    ]

-- Action

data Action
    = NoOp
    | RedrawCanvas
    | MouseDown
    | MouseUp 
    | MouseMove (Int, Int)
    | SetXy (Double, Double)
    | SetPaths [Path]
    | SendXhr Path
    | RecvSse (Maybe Path)
    | InitAllPaths
    deriving (Eq, Show)

