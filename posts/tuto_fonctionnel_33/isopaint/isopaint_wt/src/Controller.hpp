#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <map>
#include <mutex>
#include <vector>

#include <Wt/WPainter.h>
#include <Wt/WServer.h>

#include "AppDrawing.hpp"

class Controller {
    private:
        mutable std::mutex _mutex;
        std::vector<Wt::WPainterPath> _paths;
        std::map<AppDrawing*, std::string> _connections;

    public:
        // register client app
        void addClient(AppDrawing * app) {
            std::unique_lock<std::mutex> lock(_mutex);
            _connections[app] = app->instance()->sessionId();
        }

        // unregister client app
        void removeClient(AppDrawing * app) {
            std::unique_lock<std::mutex> lock(_mutex);
            _connections.erase(app);
        }

        // get all paths
        std::vector<Wt::WPainterPath> getPaths() const {
            std::unique_lock<std::mutex> lock(_mutex);
            return _paths;
        }

        // add a new path and update all client apps
        void addPath(const Wt::WPainterPath & path) {
            std::unique_lock<std::mutex> lock(_mutex);
            _paths.push_back(path);
            for (auto & conn : _connections) {
                auto updateFunc = std::bind(&AppDrawing::addPath, conn.first, path);
                Wt::WServer::instance()->post(conn.second, updateFunc);
            }
        }
};

#endif

