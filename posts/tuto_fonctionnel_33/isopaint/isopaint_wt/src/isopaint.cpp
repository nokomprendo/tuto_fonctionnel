#include "Controller.hpp"

int main(int argc, char ** argv) {

    // controller: handle client connections and data (drawn paths)
    Controller controller;

    Wt::WServer server(argc, argv, WTHTTP_CONFIGURATION);

    // endpoint "/": create a client app and register connection in the controller
    auto mkApp = [&controller] (const Wt::WEnvironment & env) {
        return std::make_unique<AppDrawing>(env, controller);
    };
    server.addEntryPoint(Wt::EntryPointType::Application, mkApp, "/");

    server.run();
    return 0;
}

