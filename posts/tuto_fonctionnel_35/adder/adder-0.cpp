#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

struct Adder {
    int _result;
    Adder(const std::vector<int> & values) {
        _result = std::accumulate(values.begin(), values.end(), 0, std::plus<int>());
    }
};

int main() {
    Adder a1({1});
    std::cout << a1._result << std::endl;
    Adder a3({1,2,3});
    std::cout << a3._result << std::endl;
    return 0;
}

