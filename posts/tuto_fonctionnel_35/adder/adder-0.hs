newtype Adder = Adder { _result :: Int } deriving Show

mkAdder :: [Int] -> Adder
mkAdder xs = Adder (sum xs)

a1, a3 :: Adder
a1 = mkAdder [1]
a3 = mkAdder [1, 2, 3]

main :: IO ()
main = do
    print a1
    print a3

