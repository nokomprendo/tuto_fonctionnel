#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

template<int ... SN> struct Adder;

template<int S> 
struct Adder<S> {
    static const int _result = S;
};

template<int S, int ... SN> 
struct Adder<S, SN ...> {
    static const int _result = S + Adder<SN...>::_result;
};

int main() {
    Adder<1> a1;
    std::cout << a1._result << std::endl;
    Adder<1,2,3> a3;
    std::cout << a3._result << std::endl;
    std::cout << Adder<1,2,3>::_result << std::endl;
    return 0;
}

