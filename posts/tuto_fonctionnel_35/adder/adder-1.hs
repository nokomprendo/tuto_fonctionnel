{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

data Z = Z

data (:.) thetail thehead = (:.) thetail thehead 

type DIM0 = Z
type DIM1 = DIM0 :. Int
type DIM2 = DIM1 :. Int

newtype Adder = Adder { _result :: Int } deriving Show

-- http://hackage.haskell.org/package/repa-3.4.1.4/docs/doc-index-All.html
-- https://github.com/haskell-repa/repa/blob/master/repa/Data/Array/Repa/Index.hs
-- https://wiki.haskell.org/Numeric_Haskell:_A_Repa_Tutorial

f :: DIM2 -> Adder
f (Z :. n2 :. n1) = Adder (n2 + n1)

main :: IO ()
main = do
    print "toto"
    print $ f (Z :. 3 :. 2)

    -- print $ mkAdder (2 :. Z)

