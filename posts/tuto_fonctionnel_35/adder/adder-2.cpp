#include <functional>
#include <iostream>
#include <numeric>
#include <vector>

template <int ... SN>
struct Adder {
    static const int _result = (0 + ... + SN);
};

int main() {
    Adder<1> a1;
    std::cout << a1._result << std::endl;
    Adder<1,2,3> a3;
    std::cout << a3._result << std::endl;
    std::cout << Adder<1,2,3>::_result << std::endl;
    return 0;
}

