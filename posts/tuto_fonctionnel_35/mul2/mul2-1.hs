mul2int :: Int -> Int
mul2int = (*2)

mul2double :: Double -> Double
mul2double = (*2)

main :: IO ()
main = do
    print $ mul2int (21::Int)
    print $ mul2double (21::Double)

