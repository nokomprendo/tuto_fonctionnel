#include <iostream>
#include <typeinfo>

template <typename T>
T mul2(T x) {
    std::cout << "T mul2(T x)... T=" << typeid(x).name() << std::endl;
    return x*2;
}

int main() {

    int n1 = 21;
    int r1 = mul2<int>(n1);
    std::cout << n1 << " " << r1 << std::endl;

    double n2 = 2.1;
    double r2 = mul2<double>(n2);
    std::cout << n2 << " " << r2 << std::endl;

    /*
    std::string n3 = "2.1";
    double r3 = mul2<std::string>(n3);
    std::cout << n3 << " " << r3 << std::endl;
    */

    return 0;
}

