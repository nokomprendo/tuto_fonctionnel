mul2 :: Num a => a -> a
mul2 = (*2)

main :: IO ()
main = do
    print $ mul2 (21::Int)
    print $ mul2 (21::Double)


