#include <iostream>
#include <typeinfo>
#include <type_traits>

template <typename T>
T mul2(T x) {
    static_assert(std::is_integral<T>::value);
    std::cout << "T mul2(T x)... T=" << typeid(x).name() << std::endl;
    return x*2;
}

int main() {

    int n1 = 21;
    int r1 = mul2<int>(n1);
    std::cout << n1 << " " << r1 << std::endl;

    /*
    double n2 = 2.1;
    double r2 = mul2<double>(n2);
    std::cout << n2 << " " << r2 << std::endl;
    */

    return 0;
}


