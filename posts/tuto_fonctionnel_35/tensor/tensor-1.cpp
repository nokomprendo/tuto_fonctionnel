#include <array>

template<size_t ... SN> struct Tensor;

template<size_t S> 
struct Tensor<S> {

    using Type = std::array<double, S>;
    Type _data;

    double & operator[](unsigned i) {
        return _data[i];
    }

    const Tensor<S> & operator+=(const Tensor<S> & t) {
        _data += t._data;
        return *this;
    }

    void fill(const double & v) {
        _data.fill(v);
    }
};

template<size_t S, size_t ... SN> 
struct Tensor<S, SN...> {

    using NestedType = Tensor<SN...>;
    using Type = std::array<NestedType, S>;
    Type _data;

    NestedType & operator[](unsigned i) {
        return _data[i];
    }

    const Tensor<S, SN...> & operator+=(const Tensor<S, SN...> & t) {
        _data += t._data;
        return *this;
    }

    void fill(const double & v) {
        for (auto & t : _data)
            t.fill(v);
    }
}; 

template <typename T, size_t S>
const std::array<T, S> & operator+=(std::array<T, S> & t1, const std::array<T, S> & t2) {
    for (unsigned i=0; i<S; i++)
        t1[i] += t2[i];
    return t1;
}

template<size_t S, size_t ... SN> 
const Tensor<S, SN...> operator*(double k, const Tensor<S, SN...> & t1) {
    Tensor<S, SN...> t2;
    t2._data = k * t1._data;
    return t2;
}

template <typename T, size_t S>
const std::array<T, S> operator*(double k, const std::array<T, S> & t1) {
    std::array<T, S> t2;
    for (unsigned i=0; i<S; i++)
        t2[i] = k * t1[i];
    return t2;
}

#include <iostream>

int main() {

    Tensor<2,3,4> t1;
    t1.fill(7.0);
    Tensor<2,3,4> t2;
    t2.fill(3.0);
    Tensor<2,3,4> t3 = 2.0 * t1;

    t1 += t2;
    std::cout << t1[1][2][3] << std::endl;
    std::cout << t2[1][2][3] << std::endl;
    std::cout << t3[1][2][3] << std::endl;

    t1[1][2][3] = 0.0;
    std::cout << t1[1][2][3] << std::endl;

    Tensor<2> t4;
    t4.fill(22.0);
    Tensor<2> t5;
    t5.fill(20.0);
    t4 += t5; 
    Tensor<2> t6 = 2.0 * t4;
    std::cout << t6[1] << std::endl;

    return 0;
}

