---
title: Introduction aux templates variadiques en C++
---

[vidéo youtube](https://youtu.be/i2IL734ps0M) - 
[vidéo peertube](https://peertube.fr/videos/watch/95192566-40fb-4c42-8541-b30a4e73ee54)

# Introduction aux templates variadiques en C++

https://jonathanpoelen.github.io/
https://github.com/kvasir-io/mpl
https://www.boost.org/doc/libs/1_70_0/libs/hana/doc/html/index.html

## GNU Guix

Le projet GNU Guix développe principalement deux outils :

- Guix, un gestionnaire de paquets transactionnel, utilisable sur une
  distribution linux classique;
- Guix System, une distribution linux basée sur le gestionnaire de paquets
  Guix.

Quelques liens :

- [GNU Guix](https://www.gnu.org/software/guix/)
- [Annonce GNU Guix 1.0.0](https://www.gnu.org/software/guix/blog/2019/gnu-guix-1.0.0-released/)
- [Guide d'installation](https://www.gnu.org/software/guix/manual/en/html_node/Guided-Graphical-Installation.html#Guided-Graphical-Installation)

# Installation

- [télécharger l'image ISO](https://www.gnu.org/software/guix/download/)

- la décompresser :

    ```bash
    xz -d guix-system-install-1.0.0.x86_64-linux.iso.xz
    ```

## Conclusion

Ce bug dans l'image d'installation est effectivement malheureux mais il ne faut
pas s'arrêter à cela car une solution est proposée dans la documentation, en
attendant que le problème soit corrigé.

Le projet mérite d'autant plus d'indulgence que c'est sa première vraie version,
qu'elle apporte des innovations importantes (gestionnaire de paquets
transactionnel, gestionnaire de services, paquets libres…) et qu'elle se destine
à des utiliseurs relativement avertis. 

