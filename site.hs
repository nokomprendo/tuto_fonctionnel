{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid (mappend)
import Hakyll


config :: Configuration
config = defaultConfiguration { destinationDirectory = "public" }


postCtx :: Context String
postCtx = dateField "date" "%Y-%m-%d" `mappend` defaultContext


main :: IO ()
main = hakyllWith config $ do

  match "css/*" $ do
    route idRoute
    compile compressCssCompiler

  match "posts/*/images/*/*" $ do
    route idRoute
    compile copyFileCompiler

  match "posts/*/images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "posts/*/data/*" $ do
    route idRoute
    compile copyFileCompiler

  match "images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "templates/*" $ compile templateBodyCompiler

  match "posts/*/*-README.md" $ do
    route $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls

  match "posts/*/*.md" $ do
    route $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*/*-README*"
      let indexCtx =
            listField "posts" postCtx (return posts) `mappend`
            defaultContext
      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls

